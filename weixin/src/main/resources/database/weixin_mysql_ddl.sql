/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.7.23 : Database - weixin
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`weixin` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `weixin`;

/*Table structure for table `wx_mp_account` */

DROP TABLE IF EXISTS `wx_mp_account`;

CREATE TABLE `wx_mp_account` (
  `app_id` char(18) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号的唯一标识（主键）',
  `wx_id` char(15) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '微信原始ID',
  `app_secret` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号密钥',
  `token` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号令牌',
  PRIMARY KEY (`app_id`),
  UNIQUE KEY `wx_id` (`wx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信公众号';

/*Table structure for table `wx_mp_business` */

DROP TABLE IF EXISTS `wx_mp_business`;

CREATE TABLE `wx_mp_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键。对应sid，商户自己的id，用于后续审核通过收到poi_id 的通知时，做对应关系。请商户自己保证唯一识别性',
  `poi_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信的门店ID微信内门店唯一标示ID',
  `app_id` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号的唯一标识（外键）',
  `business_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店名称（仅为商户名，如：国美、麦当劳，不应包含地区、地址、分店名等信息，错误示例：北京国美） 不能为空，15个汉字或30个英文字符内',
  `branch_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分店名称（不应包含地区信息，不应与门店名有重复，错误示例：北京王府井店） 20 个字 以内',
  `province` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '	门店所在的省份（直辖市填城市名,如：北京 市） 10个字 以内',
  `city` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店所在的城市 10个字 以内',
  `district` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店所在地区 10个字 以内',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店所在的详细街道地址（不要填写省市信息）',
  `telephone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '	门店的电话（纯数字，区号、分机号均由“-”隔开）',
  `offset_type` tinyint(4) NOT NULL COMMENT '坐标类型： 1 为火星坐标 2 为sogou经纬度 3 为百度经纬度 4 为mapbar经纬度 5 为GPS坐标 6 为sogou墨卡托坐标 注：高德经纬度无需转换可直接使用',
  `longitude` double NOT NULL COMMENT '门店所在地理位置的经度',
  `latitude` double NOT NULL COMMENT '门店所在地理位置的纬度（经纬度均为火星坐标，最好选用腾讯地图标记的坐标）',
  `recommend` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容 200字以内',
  `special` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '特色服务，如免费wifi，免费停车，送货上门等商户能提供的特色功能或服务',
  `introduction` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商户简介，主要介绍商户信息等 300字以内',
  `open_time` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '营业时间，24 小时制表示，用“-”连接，如 8:00-20:00',
  `avg_price` int(10) unsigned DEFAULT NULL COMMENT '人均价格，大于0 的整数',
  `poi_check_notify_result` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核结果，成功succ 或失败fail',
  `poi_check_notify_msg` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '成功的通知信息，或审核失败的驳回理由',
  PRIMARY KEY (`id`),
  KEY `idx_poi_id` (`poi_id`),
  KEY `idx_app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信门店';

/*Table structure for table `wx_mp_business_photo` */

DROP TABLE IF EXISTS `wx_mp_business_photo`;

CREATE TABLE `wx_mp_business_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `business_id` int(10) unsigned NOT NULL COMMENT '门店ID（外键）',
  `url` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片URL',
  PRIMARY KEY (`id`),
  KEY `idx_business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信门店图片';

/*Table structure for table `wx_mp_card_base_info` */

DROP TABLE IF EXISTS `wx_mp_card_base_info`;

CREATE TABLE `wx_mp_card_base_info` (
  `card_id` char(28) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '微信卡券ID（主键、外键）',
  `card_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡券类型。MEMBER_CARD-会员卡。',
  `check_state` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核状态；card_checking-卡券审核中;card_pass_check-卡券通过审核；card_not_pass_check-卡券未通过审核。',
  `refuse_reason` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核不通过原因。',
  `logo_url` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡券的商户logo，建议像素为300*300。',
  `code_type` char(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '码型： "CODE_TYPE_TEXT"文 本 ； "CODE_TYPE_BARCODE"一维码 "CODE_TYPE_QRCODE"二维码 "CODE_TYPE_ONLY_QRCODE",二维码无code显示； "CODE_TYPE_ONLY_BARCODE",一维码无code显示；CODE_TYPE_NONE， 不显示code和条形码类型',
  `brand_name` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商户名字,字数上限为12个汉字。',
  `title` varchar(27) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡券名，字数上限为9个汉字。(建议涵盖卡券属性、服务及金额)。',
  `color` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '券颜色。按色彩规范标注填写Color010-Color100。',
  `notice` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '	卡券使用提醒，字数上限为16个汉字。',
  `description` varchar(3072) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡券使用说明，字数上限为1024个汉字。',
  `sku_quantity` int(10) unsigned NOT NULL COMMENT '卡券库存的数量，上限为100000000。',
  `date_info_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用时间的类型。DATE_TYPE_FIX _TIME_RANGE 表示固定日期区间，DATETYPE FIX_TERM 表示固定时长 （自领取后按天算）。',
  `begin_timestamp` int(10) unsigned DEFAULT NULL COMMENT 'type为DATE_TYPE_FIX_TIME_RANGE时专用，表示起用时间。从1970年1月1日00:00:00至起用时间的秒数，最终需转换为字符串形态传入。（东八区时间,UTC+8，单位为秒）',
  `end_timestamp` int(10) unsigned DEFAULT NULL COMMENT '	表示结束时间 ， 建议设置为截止日期的23:59:59过期 。 （ 东八区时间,UTC+8，单位为秒 ）',
  `fixed_term` int(10) unsigned DEFAULT NULL COMMENT 'type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天内有效，不支持填写0。',
  `fixed_begin_term` int(10) unsigned DEFAULT NULL COMMENT '	type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天开始生效，领取后当天生效填写0。（单位为天）',
  `fixed_end_timestamp` int(10) unsigned DEFAULT NULL COMMENT '可用于DATE_TYPE_FIX_TERM时间类型，表示卡券统一过期时间 ， 建议设置为截止日期的23:59:59过期 。 （ 东八区时间,UTC+8，单位为秒 ），设置了fixed_term卡券，当时间达到end_timestamp时卡券统一过期',
  `get_limit` int(11) DEFAULT NULL COMMENT '每人可领券的数量限制,不填写默认为50。',
  `use_limit` int(11) DEFAULT NULL COMMENT '每人可核销的数量限制,不填写默认为50。',
  `can_share` tinyint(1) DEFAULT NULL COMMENT '卡券领取页面是否可分享。',
  `can_give_friend` tinyint(1) DEFAULT NULL COMMENT '卡券是否可转赠。',
  `is_swipe_card` tinyint(1) DEFAULT NULL COMMENT '是否设置该会员卡支持拉出微信支付刷卡界面（会员卡独有）',
  `is_pay_and_qrcode` tinyint(1) DEFAULT NULL COMMENT '是否设置该会员卡中部的按钮同时支持微信支付刷卡和会员卡二维码（会员卡独有）',
  `need_push_on_view` tinyint(1) DEFAULT NULL COMMENT '填写true为用户点击进入会员卡时推送事件，默认为false。详情见 进入会员卡事件推送（会员卡独有）',
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信卡券基础信息（必填字段）';

/*Table structure for table `wx_mp_member_card` */

DROP TABLE IF EXISTS `wx_mp_member_card`;

CREATE TABLE `wx_mp_member_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_id` char(18) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号的唯一标识（外键）',
  `card_id` char(28) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '会员卡ID',
  `background_pic_url` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '员卡自定义卡面背景图',
  `prerogative` varchar(3072) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '特权说明。',
  `auto_activate` tinyint(1) DEFAULT '0' COMMENT '是否开通自动激活 ，设置为true时用户领取会员卡自动设置为激活， 详情见 自动激活 。',
  `wx_activate` tinyint(1) DEFAULT '0' COMMENT '是否开通一键开卡 设置为true时，该卡将支持一键开卡详情见 一键开卡 。 该选项与activate_url互斥。',
  `supply_bonus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支持积分，仅支持从false变为true，默认为false',
  `bonus_url` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '积分信息类目跳转的url。',
  `supply_balance` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支持储值，仅支持从false变为true，默认为fals e 该字段须开通储值功能后方可使用， 详情见： 获取特殊权限',
  `balance_url` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '余额信息类目跳转的url。',
  `bonus_cleared` varchar(3072) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '积分清零规则。',
  `bonus_rules` varchar(3072) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '积分规则。',
  `balance_rules` varchar(3072) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '储值说明。',
  `activate_url` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '激活链接',
  `activate_app_brand_user_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '激活会原卡url对应的小程序user_name，仅可跳转该公众号绑定的小程序',
  `activate_app_brand_pass` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '激活会原卡url对应的小程序path',
  `cost_money_unit` int(10) unsigned DEFAULT NULL COMMENT '消费金额，以分为单位',
  `increase_bonus` int(10) unsigned DEFAULT NULL COMMENT '	根据以上消费金额对应增加的积分',
  `max_increase_bonus` int(10) unsigned DEFAULT NULL COMMENT '单次获取的积分上限',
  `init_increase_bonus` int(10) unsigned DEFAULT NULL COMMENT '用户激活后获得的初始积分',
  `cost_bonus_unit` int(10) unsigned DEFAULT NULL COMMENT '每使用x积分。',
  `reduce_money` int(10) unsigned DEFAULT NULL COMMENT '抵扣xx元，（这里以分为单位）',
  `least_money_to_use_bonus` int(10) unsigned DEFAULT NULL COMMENT '抵扣条件，满xx元（这里以分为单位）可用',
  `max_reduce_bonus` int(10) unsigned DEFAULT NULL COMMENT '	抵扣条件，单笔最多使用xx积分',
  `discount` int(10) unsigned DEFAULT NULL COMMENT '	折扣，该会员卡享受的折扣优惠',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_card_id` (`card_id`),
  KEY `idx_app_id` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信会员卡';

/*Table structure for table `wx_mp_user` */

DROP TABLE IF EXISTS `wx_mp_user`;

CREATE TABLE `wx_mp_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_id` char(18) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号的唯一标识（外键）',
  `subscribe` tinyint(4) NOT NULL COMMENT '用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息',
  `subscribe_time` datetime DEFAULT NULL COMMENT '用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间',
  `unsubscribe_time` datetime DEFAULT NULL COMMENT '用户取消关注时间',
  `openid` char(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户的标识，对当前公众号唯一',
  `nickname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户昵称',
  `sex` tinyint(4) DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `country` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在国家，如中国为CN',
  `province` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在省份,用户个人资料填写的省份',
  `city` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户所在城市,普通用户个人资料填写的城市',
  `language` char(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `headimgurl` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。',
  `unionid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。',
  `remark` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注',
  `privilege` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）',
  `groupid` int(11) DEFAULT NULL COMMENT '用户所在的分组ID（兼容旧的用户分组接口）',
  `subscribe_scene` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'subscribe_scene	返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENEPROFILE LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他',
  `qr_scene` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '二维码扫码场景（开发者自定义）',
  `qr_scene_str` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '二维码扫码场景描述（开发者自定义）',
  PRIMARY KEY (`id`),
  KEY `idx_app_id` (`app_id`),
  KEY `idx_openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信公众号用户';

/*Table structure for table `wx_mp_user_card` */

DROP TABLE IF EXISTS `wx_mp_user_card`;

CREATE TABLE `wx_mp_user_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_id` char(18) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公众号的唯一标识（外键）',
  `wx_id` char(15) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '微信原始ID',
  `openid` char(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户的标识，对当前公众号唯一',
  `card_id` char(28) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡券ID',
  `card_state` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '卡券状态。normal-正常；delete-删除。',
  `is_give_by_friend` tinyint(1) DEFAULT NULL COMMENT '是否为转赠领取，1代表是，0代表否。',
  `friend_user_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	当IsGiveByFriend为1时填入的字段，表示发起转赠用户的openid',
  `user_card_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'code序列号。',
  `old_user_card_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '为保证安全，微信会在转赠发生后变更该卡券的code号，该字段表示转赠前的code。',
  `outer_str` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	领取场景值，用于领取渠道数据统计。可在生成二维码接口及添加Addcard接口中自定义该字段的字符串值。',
  `is_restore_member_card` tinyint(1) DEFAULT NULL COMMENT '用户删除会员卡后可重新找回，当用户本次操作为找回时，该值为1，否则为0',
  `unionid` char(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '领券用户的UnionId',
  `outer_id` int(11) DEFAULT NULL,
  `is_recommend_by_friend` int(1) DEFAULT NULL,
  `source_scene` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='微信公众号用户领取的会员卡';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
