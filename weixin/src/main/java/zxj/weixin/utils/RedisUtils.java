package zxj.weixin.utils;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Redis工具类
 * 
 * @author zhuxuejiang
 *
 */
@Component
public class RedisUtils<K, V> {

	@Autowired
	private RedisTemplate<K, V> redisTemplate;

	/**
	 * 查询键是否存在
	 * 
	 * @param key 键
	 * @return true-存在；false-不存在
	 */
	public boolean hasKey(K key) {
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置键值对的有效期
	 * 
	 * @param key     键
	 * @param timeout 有效期（单位：秒）
	 */
	public void expire(K key, long timeout) {
		redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 添加键值对
	 * 
	 * @param key   键
	 * @param value 值
	 */
	public void opsForValueSet(K key, V value) {
		redisTemplate.opsForValue().set(key, value);
	}

	/**
	 * 添加键值对
	 * 
	 * @param key     键
	 * @param value   值
	 * @param timeout 有效期（单位：秒）
	 */
	public void opsForValueSet(K key, V value, long timeout) {
		redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 获取字符串
	 * 
	 * @param key 键
	 * @return 值对象
	 */
	public V opsForValueGet(K key) {
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 将一个元素添加到Set中
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Long opsForSetAdd(K key, V value) {
		return redisTemplate.opsForSet().add(key, value);
	}

	/**
	 * 将一个元素从Set中移除
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public Long opsForSetRemove(K key, V value) {
		return redisTemplate.opsForSet().remove(key, value);
	}

	/**
	 * 判断一个元素是否存在与Set中
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public Boolean opsForSetIsMember(K key, V value) {
		return redisTemplate.opsForSet().isMember(key, value);
	}

}
