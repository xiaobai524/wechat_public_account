package zxj.weixin.utils;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Redis工具类
 * 
 * @author zhuxuejiang
 *
 */
@Component
public class RedisStringUtils {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	/**
	 * 查询键是否存在
	 * 
	 * @param key 键
	 * @return true-存在；false-不存在
	 */
	public boolean hasKey(String key) {
		return stringRedisTemplate.hasKey(key);
	}

	/**
	 * 设置键值对的有效期
	 * 
	 * @param key     键
	 * @param timeout 有效期（单位：秒）
	 */
	public void expire(String key, long timeout) {
		stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 添加键值对
	 * 
	 * @param key   键
	 * @param value 值
	 */
	public void opsForValueSet(String key, String value) {
		stringRedisTemplate.opsForValue().set(key, value);
	}

	/**
	 * 添加键值对
	 * 
	 * @param key     键
	 * @param value   值
	 * @param timeout 有效期（单位：秒）
	 */
	public void opsForValueSet(String key, String value, long timeout) {
		stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 获取字符串
	 * 
	 * @param key 键
	 * @return 字符串
	 */
	public String opsForValueGet(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

	/**
	 * 将一个元素添加到Set中
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public Long opsForSetAdd(String key, String value) {
		return stringRedisTemplate.opsForSet().add(key, value);
	}

	/**
	 * 将一个元素从Set中移除
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public Long opsForSetRemove(String key, String value) {
		return stringRedisTemplate.opsForSet().remove(key, value);
	}

	/**
	 * 判断一个元素是否存在与Set中
	 * 
	 * @param key   键
	 * @param value 值
	 * @return
	 */
	public Boolean opsForSetIsMember(String key, String value) {
		return stringRedisTemplate.opsForSet().isMember(key, value);
	}

}
