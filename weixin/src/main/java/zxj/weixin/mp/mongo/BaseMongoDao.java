package zxj.weixin.mp.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * MongoDB DAO基类
 * 
 * @author zhuxuejiang
 *
 * @param <T> 实体类型
 * @param <K> 主键类型
 */
public abstract class BaseMongoDao<T, K> {

	@Autowired
	protected MongoTemplate mongoTemplate;

	public void save(T entity) {
		mongoTemplate.save(entity);
	}

}
