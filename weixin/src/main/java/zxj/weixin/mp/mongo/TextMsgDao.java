package zxj.weixin.mp.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import zxj.weixin.mp.domain.message.TextMsg;

/**
 * 微信文本消息DAO
 * 
 * @author zhuxuejiang
 *
 */
@Component
public class TextMsgDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public void save(TextMsg entity) {
		mongoTemplate.save(entity);
	}

}
