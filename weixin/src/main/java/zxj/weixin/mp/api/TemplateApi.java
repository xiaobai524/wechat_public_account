package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信模版消息API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class TemplateApi {

	private static final String ACCESS_TOKEN = "{access_token}";

	/** 设置所属行业接口路径 */
	private static final String SET_INDUSTRY_URL = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={access_token}";

	/** 获取设置的行业信息接口路径 */
	private static final String GET_INDUSTRY_URL = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token={access_token}";

	/** 获得模板ID接口路径 */
	private static final String ADD_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={access_token}";

	/** 获取模板列表接口路径 */
	private static final String GET_ALL_PRIVATE_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={access_token}";

	/** 删除模版接口路径 */
	private static final String DEL_PRIVATE_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token={access_token}";

	/** 发送模板消息接口路径 */
	private static final String SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={access_token}";

	/**
	 * 设置所属行业
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> setIndustry(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(SET_INDUSTRY_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取设置的行业信息
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getIndustry(String accessToken) throws Exception {
		String response = HttpClientUtils.get(GET_INDUSTRY_URL.replace(ACCESS_TOKEN, accessToken));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获得模板ID
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> addTemplate(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(ADD_TEMPLATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取模板列表
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getAllPrivateTemplate(String accessToken) throws Exception {
		String response = HttpClientUtils.get(GET_ALL_PRIVATE_TEMPLATE_URL.replace(ACCESS_TOKEN, accessToken));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 删除模版
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> delPrivateTemplate(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(DEL_PRIVATE_TEMPLATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 发送模板消息
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> send(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(SEND_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

}
