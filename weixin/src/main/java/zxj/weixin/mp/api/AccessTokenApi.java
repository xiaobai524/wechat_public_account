package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信公众号基础功能API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class AccessTokenApi {

	private static final String APP_ID = "{appid}";
	private static final String SECRET = "{secret}";

	/** 获取access_token接口路径 */
	private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={secret}";

	/**
	 * 获取access_token
	 * 
	 * @param appId     公众号的唯一标识
	 * @param appSecret 公众号密钥
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getAccessToken(String appId, String appSecret) throws Exception {
		String response = HttpClientUtils.get(ACCESS_TOKEN_URL.replace(APP_ID, appId).replace(SECRET, appSecret));
		return MapUtils.jsonString2Map(response);
	}

}
