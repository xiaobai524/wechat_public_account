package zxj.weixin.mp.api.entity;

/**
 * 微信会员卡用户信息
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCardUserInfo {

	/** 用户在本公众号内唯一识别码 */
	private String openid;

	/** 用户昵称 */
	private String nickname;

	/** 会员卡号 */
	private String membershipNumber;

	/** 积分信息 */
	private Integer bonus;

	/** 余额信息 */
	private Double balance;

	/** 用户性别 */
	private String sex;

	/** 手机号 */
	private String mobileCommonField;

	/** 性别 */
	private String sexCommonField;

	/** 姓名 */
	private String nameCommonField;

	/** 生日 */
	private String birthdayCommonField;

	/** 身份证 */
	private String idcardCommonField;

	/** 邮箱 */
	private String emailCommonField;

	/** 详细地址 */
	private String locationCommonField;

	/** 教育背景 */
	private String educationBackgroCommonField;

	/** 行业 */
	private String industryCommonField;

	/** 收入 */
	private String incomeCommonField;

	/** 兴趣爱好 */
	private String habitCommonField;

	/**
	 * 当前用户会员卡状态，
	 * NORMAL 正常
	 * EXPIRE 已过期
	 * GIFTING 转赠中
	 * GIFT_SUCC 转赠成功
	 * GIFT_TIMEOUT 转赠超时
	 * DELETE 已删除，
	 * UNAVAILABLE 已失效
	 */
	private String userCardStatus;

	/** 该卡是否已经被激活，true表示已经被激活，false表示未被激活 */
	private Boolean hasActive;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMembershipNumber() {
		return membershipNumber;
	}

	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}

	public Integer getBonus() {
		return bonus;
	}

	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMobileCommonField() {
		return mobileCommonField;
	}

	public void setMobileCommonField(String mobileCommonField) {
		this.mobileCommonField = mobileCommonField;
	}

	public String getSexCommonField() {
		return sexCommonField;
	}

	public void setSexCommonField(String sexCommonField) {
		this.sexCommonField = sexCommonField;
	}

	public String getNameCommonField() {
		return nameCommonField;
	}

	public void setNameCommonField(String nameCommonField) {
		this.nameCommonField = nameCommonField;
	}

	public String getBirthdayCommonField() {
		return birthdayCommonField;
	}

	public void setBirthdayCommonField(String birthdayCommonField) {
		this.birthdayCommonField = birthdayCommonField;
	}

	public String getIdcardCommonField() {
		return idcardCommonField;
	}

	public void setIdcardCommonField(String idcardCommonField) {
		this.idcardCommonField = idcardCommonField;
	}

	public String getEmailCommonField() {
		return emailCommonField;
	}

	public void setEmailCommonField(String emailCommonField) {
		this.emailCommonField = emailCommonField;
	}

	public String getLocationCommonField() {
		return locationCommonField;
	}

	public void setLocationCommonField(String locationCommonField) {
		this.locationCommonField = locationCommonField;
	}

	public String getEducationBackgroCommonField() {
		return educationBackgroCommonField;
	}

	public void setEducationBackgroCommonField(String educationBackgroCommonField) {
		this.educationBackgroCommonField = educationBackgroCommonField;
	}

	public String getIndustryCommonField() {
		return industryCommonField;
	}

	public void setIndustryCommonField(String industryCommonField) {
		this.industryCommonField = industryCommonField;
	}

	public String getIncomeCommonField() {
		return incomeCommonField;
	}

	public void setIncomeCommonField(String incomeCommonField) {
		this.incomeCommonField = incomeCommonField;
	}

	public String getHabitCommonField() {
		return habitCommonField;
	}

	public void setHabitCommonField(String habitCommonField) {
		this.habitCommonField = habitCommonField;
	}

	public String getUserCardStatus() {
		return userCardStatus;
	}

	public void setUserCardStatus(String userCardStatus) {
		this.userCardStatus = userCardStatus;
	}

	public Boolean getHasActive() {
		return hasActive;
	}

	public void setHasActive(Boolean hasActive) {
		this.hasActive = hasActive;
	}

	@Override
	public String toString() {
		return "MemberCardUserInfo [openid=" + openid + ", nickname=" + nickname + ", membershipNumber="
				+ membershipNumber + ", bonus=" + bonus + ", balance=" + balance + ", sex=" + sex
				+ ", mobileCommonField=" + mobileCommonField + ", sexCommonField=" + sexCommonField
				+ ", nameCommonField=" + nameCommonField + ", birthdayCommonField=" + birthdayCommonField
				+ ", idcardCommonField=" + idcardCommonField + ", emailCommonField=" + emailCommonField
				+ ", locationCommonField=" + locationCommonField + ", educationBackgroCommonField="
				+ educationBackgroCommonField + ", industryCommonField=" + industryCommonField + ", incomeCommonField="
				+ incomeCommonField + ", habitCommonField=" + habitCommonField + ", userCardStatus=" + userCardStatus
				+ ", hasActive=" + hasActive + "]";
	}

}
