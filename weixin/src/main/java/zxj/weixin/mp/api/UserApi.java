package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信用户API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class UserApi {

	private static final String ACCESS_TOKEN = "{access_token}";
	private static final String OPENID = "{openid}";

	/** 获取用户基本信息（包括UnionID机制）接口路径 */
	private static final String USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={access_token}&openid={openid}&lang=zh_CN";

	/**
	 * 获取用户基本信息
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param openid      普通用户的标识，对当前公众号唯一
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getUserInfo(String accessToken, String openid) throws Exception {
		String response = HttpClientUtils.get(USER_INFO_URL.replace(ACCESS_TOKEN, accessToken).replace(OPENID, openid));
		return MapUtils.jsonString2Map(response);
	}

}
