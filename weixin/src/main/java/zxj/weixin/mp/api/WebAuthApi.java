package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信网页授权Service
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class WebAuthApi {

	private static final String APP_ID = "{appid}";
	private static final String SECRET = "{secret}";
	private static final String CODE = "{code}";
	private static final String REFRESH_TOKEN = "{refresh_token}";
	private static final String ACCESS_TOKEN = "{access_token}";
	private static final String OPENID = "{openid}";

	/** 第二步：通过code换取网页授权access_token接口路径 */
	private static final String OAUTH2_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={appid}&secret={secret}&code={code}&grant_type=authorization_code";

	/** 第三步：刷新access_token（如果需要） */
	private static final String OAUTH2_REFRESH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={appid}&grant_type=refresh_token&refresh_token= {refresh_token}";

	/** 第四步：拉取用户信息(需scope为 snsapi_userinfo) */
	private static final String USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token= {access_token}&openid={openid}&lang=zh_CN";

	/** 检验授权凭证（access_token）是否有效 */
	private static final String AUTH_URL = "https://api.weixin.qq.com/sns/auth?access_token={access_token}&openid={openid}";

	/**
	 * 通过code换取网页授权access_token
	 * 
	 * @param appId     公众号的唯一标识
	 * @param appSecret 公众号密钥
	 * @param code      换取网页授权access_token的票据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getAccessToken(String appId, String appSecret, String code) throws Exception {
		String response = HttpClientUtils
				.get(OAUTH2_ACCESS_TOKEN_URL.replace(APP_ID, appId).replace(SECRET, appSecret).replace(CODE, code));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 刷新access_token
	 * 
	 * @param appId        公众号的唯一标识
	 * @param refreshToken 填写通过access_token获取到的refresh_token参数
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> refreshToken(String appId, String refreshToken) throws Exception {
		String response = HttpClientUtils
				.get(OAUTH2_REFRESH_TOKEN_URL.replace(APP_ID, appId).replace(REFRESH_TOKEN, refreshToken));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 拉取用户信息
	 * 
	 * @param accessToken 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @param openid      用户的标识，对当前公众号唯一
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getUserInfo(String accessToken, String openid) throws Exception {
		String response = HttpClientUtils.get(USER_INFO_URL.replace(ACCESS_TOKEN, accessToken).replace(OPENID, openid));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 检验授权凭证（access_token）是否有效
	 * 
	 * @param accessToken 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @param openid      用户的标识，对当前公众号唯一
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> isValidAccessToken(String accessToken, String openid) throws Exception {
		String response = HttpClientUtils.get(AUTH_URL.replace(ACCESS_TOKEN, accessToken).replace(OPENID, openid));
		return MapUtils.jsonString2Map(response);
	}

}
