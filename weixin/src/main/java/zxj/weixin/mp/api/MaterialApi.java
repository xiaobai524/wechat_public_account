package zxj.weixin.mp.api;

import java.io.File;
import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信公众号素材管理API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class MaterialApi {

	private static final String ACCESS_TOKEN = "{access_token}";

	/** 上传图文消息内的图片获取URL接口路径 */
	private static final String MEDIA_UPLOAD_IMG_URL = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token={access_token}";

	/**
	 * 上传图文消息内的图片获取URL
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param imgFile     图片文件
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> mediaUploadImg(String accessToken, File imgFile) throws Exception {
		String response = HttpClientUtils.post(MEDIA_UPLOAD_IMG_URL.replace(ACCESS_TOKEN, accessToken), imgFile);
		return MapUtils.jsonString2Map(response);
	}

}
