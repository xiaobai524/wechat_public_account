package zxj.weixin.mp.api;

import java.util.Map;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信卡券API
 * 
 * @author zhuxuejiang
 *
 */
public class CardApi {

	private static final String ACCESS_TOKEN = "{access_token}";

	/* 创建卡券 */

	/** 创建卡券接口路径 */
	private static final String CREATE_URL = "https://api.weixin.qq.com/card/create?access_token={access_token}";

	/* 投放卡券 */

	/** 投放卡券-创建二维码接口路径 */
	private static final String QRCODE_CREATE_URL = "https://api.weixin.qq.com/card/qrcode/create?access_token={access_token}";

	/** 投放卡券-通过卡券货架投放卡券-创建货架接口路径 */
	private static final String LANDING_PAGE_CREATE_URL = "https://api.weixin.qq.com/card/landingpage/create?access_token={access_token}";

	/** 投放卡券-群发卡券-图文消息群发卡券接口路径 */
	private static final String MP_NEWS_GET_HTML_URL = "https://api.weixin.qq.com/card/mpnews/gethtml?access_token={access_token}";

	/** 设置测试白名单接口路径 */
	private static final String TEST_WHITE_LIST_SET_URL = "https://api.weixin.qq.com/card/testwhitelist/set?access_token={access_token}";

	/* 管理卡券 */

	/** 获取用户已领取卡券接口路径 */
	private static final String USER_GET_CARD_LIST_URL = "https://api.weixin.qq.com/card/user/getcardlist?access_token={access_token}";

	/** 批量查询卡券列表接口路径 */
	private static final String BATCH_GET_URL = "https://api.weixin.qq.com/card/batchget?access_token={access_token}";

	/** 更改卡券信息接口路径 */
	private static final String UPDATE_URL = "https://api.weixin.qq.com/card/update?access_token={access_token}";

	/** 删除卡券接口路径 */
	private static final String DELETE_URL = "https://api.weixin.qq.com/card/delete?access_token={access_token}";

	/* 创建会员卡 */

	/** 设置开卡字段接口路径 */
	private static final String MEMBER_CARD_ACTIVATE_USER_FORM_SET_URL = "https://api.weixin.qq.com/card/membercard/activateuserform/set?access_token={access_token}";

	/** 获取开卡组件链接接口路径 */
	private static final String MEMBER_CARD_ACTIVATE_GET_URL = "https://api.weixin.qq.com/card/membercard/activate/geturl?access_token={access_token}";

	/* 管理会员卡 */

	/** 接口激活路径 */
	private static final String MEMBER_CARD_ACTIVATE_URL = "https://api.weixin.qq.com/card/membercard/activate?access_token={access_token}";

	/** 拉取会员信息（积分查询）接口路径 */
	private static final String MEMBER_CARD_USER_INFO_GET_URL = "https://api.weixin.qq.com/card/membercard/userinfo/get?access_token={access_token}";

	/** 更新会员信息接口路径 */
	private static final String MEMBER_CARD_UPDATE_USER_URL = "https://api.weixin.qq.com/card/membercard/updateuser?access_token={access_token}";

	/**
	 * 创建卡券
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> create(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(CREATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 创建二维码
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> createQrCode(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(QRCODE_CREATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 通过卡券货架投放卡券-创建货架
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> createLandingpage(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(LANDING_PAGE_CREATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取图文消息群发卡券html
	 * 
	 * 支持开发者调用该接口获取卡券嵌入图文消息的标准格式代码，将返回代码填入上传图文素材接口中content字段，即可获取嵌入卡券的图文消息素材。
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getHtmlMpNews(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(MP_NEWS_GET_HTML_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 设置测试白名单
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> setTestWhiteList(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(TEST_WHITE_LIST_SET_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取用户已领取卡券
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getUserCardList(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(USER_GET_CARD_LIST_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 批量查询卡券列表
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> batchGet(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(BATCH_GET_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 更改卡券信息
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> update(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(UPDATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 删除卡券接口
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> delete(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(DELETE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 设置会员卡开卡字段
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> setMemberCardActivateUserForm(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(
				MEMBER_CARD_ACTIVATE_USER_FORM_SET_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取开卡组件链接
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getMemberCardActivateUrl(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(MEMBER_CARD_ACTIVATE_GET_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 激活会员卡
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> activateMemberCard(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(MEMBER_CARD_ACTIVATE_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 拉取会员信息（积分查询）
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getMemberCardUserInfo(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(MEMBER_CARD_USER_INFO_GET_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 更新会员信息
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> updateMemberCardUser(String accessToken, Map<String, Object> dataMap)
			throws Exception {
		String response = HttpClientUtils.post(MEMBER_CARD_UPDATE_USER_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

}
