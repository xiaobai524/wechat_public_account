package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信JS-SDK API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class JsSdkApi {

	private static final String ACCESS_TOKEN = "{access_token}";

	/** 获取jsapi_ticket接口路径 */
	private static final String JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={access_token}&type=jsapi";

	/** 投放卡券-HTML5线上发券（JS-SDK接口） 获取api_ticket接口路径 */
	private static final String WX_CARD_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={access_token}&type=wx_card";

	/**
	 * 获取jsapi_ticket
	 * 
	 * @param appId       公众号的唯一标识
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getJsapiTicket(String appId, String accessToken) throws Exception {
		String response = HttpClientUtils.get(JSAPI_TICKET_URL.replace(ACCESS_TOKEN, accessToken));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取api_ticket调用卡券相关接口的临时票据
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> getWxCardTicket(String accessToken) throws Exception {
		String response = HttpClientUtils.get(WX_CARD_TICKET_URL.replace(ACCESS_TOKEN, accessToken));
		return MapUtils.jsonString2Map(response);
	}

}
