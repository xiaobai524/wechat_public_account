package zxj.weixin.mp.api;

import java.util.Map;

import org.springframework.stereotype.Service;

import zxj.weixin.utils.HttpClientUtils;
import zxj.weixin.utils.MapUtils;

/**
 * 微信门店API
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class BusinessApi {

	private static final String ACCESS_TOKEN = "{access_token}";

	/** 创建门店接口路径 */
	private static final String ADD_POI_URL = "http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token={access_token}";

	/** 门店类目表接口路径 */
	private static final String GET_WX_CATEGORY_URL = "http://api.weixin.qq.com/cgi-bin/poi/getwxcategory?access_token={access_token}";

	/**
	 * 创建门店
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @param dataMap     POST数据
	 * @return 接口返回的数据包
	 * @throws Exception
	 */
	public static Map<String, Object> addPoi(String accessToken, Map<String, Object> dataMap) throws Exception {
		String response = HttpClientUtils.post(ADD_POI_URL.replace(ACCESS_TOKEN, accessToken),
				MapUtils.map2JsonString(dataMap));
		return MapUtils.jsonString2Map(response);
	}

	/**
	 * 获取门店类目表
	 * 
	 * @param accessToken 公众号的全局唯一接口调用凭据
	 * @return categoryList 门店类目列表
	 * @throws Exception
	 */
	public static Map<String, Object> getWxCategory(String accessToken) throws Exception {
		String response = HttpClientUtils.get(GET_WX_CATEGORY_URL.replace(ACCESS_TOKEN, accessToken));
		return MapUtils.jsonString2Map(response);
	}

}
