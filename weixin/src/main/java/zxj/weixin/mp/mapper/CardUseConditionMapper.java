package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardUseCondition;

/**
 * 微信卡券高级信息-使用门槛Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardUseConditionMapper extends BaseMapper<CardUseCondition, String> {
}