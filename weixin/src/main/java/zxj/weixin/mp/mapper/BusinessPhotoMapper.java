package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.BusinessPhoto;

/**
 * 微信门店图片Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface BusinessPhotoMapper extends BaseMapper<BusinessPhoto, Integer> {

}