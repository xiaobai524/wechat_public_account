package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.UserCard;
import zxj.weixin.mp.domain.vo.UserCardVO;

/**
 * 微信公众号用户领取的会员卡Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface UserCardMapper extends BaseMapper<UserCard, Integer> {

	/**
	 * 查询微信公众号用户领取的会员卡
	 * 
	 * @param vo 查询参数对象
	 * @return 微信公众号用户领取的会员卡实体
	 */
	UserCard selectObject(UserCardVO vo);

}