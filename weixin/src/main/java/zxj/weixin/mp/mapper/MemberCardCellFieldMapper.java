package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.MemberCardCellField;

/**
 * 微信会员卡自定义信息类目Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface MemberCardCellFieldMapper extends BaseMapper<MemberCardCellField, Integer> {
}