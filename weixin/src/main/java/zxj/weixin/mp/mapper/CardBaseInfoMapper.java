package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardBaseInfo;

/**
 * 微信卡券基础信息Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardBaseInfoMapper extends BaseMapper<CardBaseInfo, String> {
}