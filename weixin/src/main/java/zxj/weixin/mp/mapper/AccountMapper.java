package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.Account;

/**
 * 微信公众号Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface AccountMapper extends BaseMapper<Account, String> {

	/**
	 * 查询微信公众号实体
	 * 
	 * @param wxId 微信原始ID
	 * @return 微信公众号实体
	 */
	Account selectByWxId(String wxId);

}
