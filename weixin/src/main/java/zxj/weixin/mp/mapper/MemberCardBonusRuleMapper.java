package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.MemberCardBonusRule;

/**
 * 微信会员卡积分规则Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface MemberCardBonusRuleMapper extends BaseMapper<MemberCardBonusRule, Integer> {
}