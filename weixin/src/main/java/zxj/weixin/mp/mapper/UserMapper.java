package zxj.weixin.mp.mapper;

import org.apache.ibatis.annotations.Param;

import zxj.weixin.mp.domain.User;

/**
 * 微信用户Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface UserMapper extends BaseMapper<User, Integer> {

	/**
	 * 查询微信用户
	 * 
	 * @param appId  公众号的唯一标识
	 * @param openid 用户的标识，对当前公众号唯一
	 * @return 微信用户实体
	 */
	User selectByOpenid(@Param("appId") String appId, @Param("openid") String openid);
}