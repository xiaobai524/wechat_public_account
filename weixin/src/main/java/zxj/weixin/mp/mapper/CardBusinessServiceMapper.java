package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardBusinessService;

/**
 * 微信卡券高级信息-商家服务Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardBusinessServiceMapper extends BaseMapper<CardBusinessService, Integer> {
}