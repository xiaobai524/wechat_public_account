package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardTimeLimit;

/**
 * 微信卡券高级信息-使用时段限制Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardTimeLimitMapper extends BaseMapper<CardTimeLimit, Integer> {
}