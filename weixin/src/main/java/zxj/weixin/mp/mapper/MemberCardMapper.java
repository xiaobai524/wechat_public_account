package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.MemberCard;
import zxj.weixin.mp.domain.vo.MemberCardVO;

/**
 * 微信会员卡Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface MemberCardMapper extends BaseMapper<MemberCard, Integer> {

	/**
	 * 查询微信会员卡
	 * 
	 * @param vo 查询参数对象
	 * @return 微信会员卡实体
	 */
	MemberCard selectObject(MemberCardVO vo);

}