package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardAbstract;

/**
 * 微信卡券高级信息-封面摘要Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardAbstractMapper extends BaseMapper<CardAbstract, String> {
}