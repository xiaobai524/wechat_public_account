package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.Business;

/**
 * 微信门店Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface BusinessMapper extends BaseMapper<Business, Integer> {
}