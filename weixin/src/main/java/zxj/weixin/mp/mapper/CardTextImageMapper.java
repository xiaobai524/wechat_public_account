package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.CardTextImage;

/**
 * 微信卡券高级信息-图文Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface CardTextImageMapper extends BaseMapper<CardTextImage, Integer> {
}