package zxj.weixin.mp.mapper;

/**
 * 通用方法Mapper
 * 
 * @author zhuxuejiang
 *
 * @param <T> 实体类型
 * @param <K> 主键类型
 */
public interface BaseMapper<T, K> {

	/**
	 * 添加记录
	 * 
	 * @param record 记录实体
	 * @return 操作成功记录数量
	 */
	int insert(T record);

	/**
	 * 添加记录，选择性的插入字段
	 * 
	 * @param record 记录实体
	 * @return 操作成功记录数量
	 */
	int insertSelective(T record);

	/**
	 * 更新记录
	 * 
	 * @param record 记录实体
	 * @return 操作成功记录数量
	 */
	int updateByPrimaryKey(T record);

	/**
	 * 更新记录，选择性的更新字段
	 * 
	 * @param record 记录实体
	 * @return
	 */
	int updateByPrimaryKeySelective(T record);

	/**
	 * 删除记录
	 * 
	 * @param id 主键
	 * @return 操作成功记录数量
	 */
	int deleteByPrimaryKey(K id);

	/**
	 * 查询记录
	 * 
	 * @param id 主键
	 * @return 实体
	 */
	T selectByPrimaryKey(K id);

}
