package zxj.weixin.mp.mapper;

import zxj.weixin.mp.domain.MemberCardCustomField;

/**
 * 微信会员卡自定义信息类目Mapper
 * 
 * @author zhuxuejiang
 *
 */
public interface MemberCardCustomFieldMapper extends BaseMapper<MemberCardCustomField, Integer> {
}