package zxj.weixin.mp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zxj.weixin.mp.domain.UserCard;
import zxj.weixin.mp.domain.vo.UserCardVO;
import zxj.weixin.mp.mapper.UserCardMapper;

/**
 * 微信公众号用户领取的会员卡Service
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class UserCardService {

	@Autowired
	private UserCardMapper userCardMapper;

	/**
	 * 查询微信公众号用户领取的会员卡
	 * 
	 * @param cardId       卡券ID
	 * @param userCardCode code序列号
	 * @return 微信公众号用户领取的会员卡实体
	 */
	public UserCard find(String cardId, String userCardCode) {
		UserCardVO vo = new UserCardVO();
		vo.setCardId(cardId);
		vo.setUserCardCode(userCardCode);

		return userCardMapper.selectObject(vo);
	}

}
