package zxj.weixin.mp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zxj.weixin.mp.api.BusinessApi;
import zxj.weixin.mp.domain.Business;
import zxj.weixin.mp.domain.BusinessPhoto;
import zxj.weixin.mp.domain.message.PoiCheckNotifyEventMsg;
import zxj.weixin.mp.mapper.BusinessMapper;
import zxj.weixin.mp.mapper.BusinessPhotoMapper;
import zxj.weixin.utils.MapUtils;

/**
 * 微信门店Service
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class BusinessService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BusinessService.class);

	@Autowired
	private BusinessMapper businessMapper;

	@Autowired
	private BusinessPhotoMapper businessPhotoMapper;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 创建门店
	 * 
	 * @param business     门店实体
	 * @param photoUrlList 门店图片URL列表
	 * @throws Exception
	 */
	public void createBusiness(Business business, List<String> categorieList, List<String> photoUrlList)
			throws Exception {

		// 判断门店实体是否为空
		if (null == business) {
			LOGGER.error("创建微信门店错误：Business is null !");
			return;
		}

		// 判断appId是否为空
		String appId = business.getAppId();
		if (StringUtils.isBlank(appId)) {
			LOGGER.error("创建微信门店错误：appId is blank !");
			return;
		}

		// 判断accessToken是否为空
		String accessToken = weixinService.getAccessToken(appId);
		if (StringUtils.isBlank(appId)) {
			LOGGER.error("创建微信门店错误：accessToken is blank !");
			return;
		}

		// 组装POST请求数据
		Map<String, Object> businessMap = new HashMap<>(16);
		businessMap.put("base_info", MapUtils.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(business)));
		businessMap.put("categories", categorieList);

		List<Map<String, Object>> photoList = new ArrayList<>(photoUrlList.size());
		for (String photoUrl : photoUrlList) {
			Map<String, Object> photoUrlMap = new HashMap<>(16);
			photoUrlMap.put("photo_url", photoUrl);
			photoList.add(photoUrlMap);
		}

		businessMap.put("photo_list", photoList);

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("business", businessMap);

		// 调用创建微信门店接口
		Map<String, Object> resultMap = BusinessApi.addPoi(accessToken, dataMap);
		if (null == resultMap) {
			LOGGER.error("创建微信门店错误 !");
			LOGGER.error("resultMap=" + resultMap);
			return;
		}

		String poiId = (String) resultMap.get("poi_id");
		if (StringUtils.isBlank(poiId)) {
			LOGGER.error("创建微信门店错误: poiId is blank !");
			return;
		}

		// 持久化微信门店
		business.setPoiId(poiId);
		businessMapper.insertSelective(business);

		// 持久化微信门店图片
		for (String url : photoUrlList) {
			BusinessPhoto photo = new BusinessPhoto();
			photo.setBusinessId(business.getId());
			photo.setUrl(url);
			businessPhotoMapper.insertSelective(photo);
		}

		return;
	}

	/**
	 * 处理审核事件推送消息
	 * 
	 * @param msg 审核事件推送消息
	 */
	public void checkNotify(PoiCheckNotifyEventMsg msg) {
		if (null == msg) {
			LOGGER.error("处理审核事件推送消息错误: PoiCheckNotifyEventMsg is blank !");
			return;
		}

		Business business = new Business();
		business.setId(Integer.parseInt(msg.getUniqId()));
		business.setPoiCheckNotifyResult(msg.getResult());
		business.setPoiCheckNotifyMsg(msg.getMsg());

		businessMapper.updateByPrimaryKeySelective(business);
	}

	/**
	 * 获取门店类目列表
	 * 
	 * @param appId 公众号的唯一标识
	 * @return 门店类目列表
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<String> getCategoryList(String appId) throws Exception {
		// 判断appId是否为空
		if (StringUtils.isBlank(appId)) {
			LOGGER.error("获取门店类目列表错误：appId is blank !");
			return null;
		}

		// 判断accessToken是否为空
		String accessToken = weixinService.getAccessToken(appId);
		if (StringUtils.isBlank(appId)) {
			LOGGER.error("获取门店类目列表错误：accessToken is blank !");
			return null;
		}

		Map<String, Object> map = BusinessApi.getWxCategory(accessToken);
		if (null == map) {
			return null;
		}

		return (List<String>) map.get("category_list");
	}

}
