package zxj.weixin.mp.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import zxj.weixin.mp.api.TemplateApi;

/**
 * 微信模版消息Service
 * 
 * @author zhuxuejiang
 *
 */
@Service
public class TemplateService {

	private static final String TITLE = "{title}";

	@Value("${weixin.template.memberRegisterSuccessNotice.id}")
	private String memberRegisterSuccessNoticeId;

	@Value("${weixin.template.memberRegisterSuccessNotice.first}")
	private String memberRegisterSuccessNoticeFirst;

	@Value("${weixin.template.memberRegisterSuccessNotice.remark}")
	private String memberRegisterSuccessNoticeRemark;

	@Value("${weixin.template.memberCardActivationSuccessNotice.id}")
	private String memberCardActivationSuccessNoticeId;

	@Value("${weixin.template.memberCardActivationSuccessNotice.first}")
	private String memberCardActivationSuccessNoticeFirst;;

	@Value("${weixin.template.memberCardActivationSuccessNotice.remark}")
	private String memberCardActivationSuccessNoticeRemark;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 发送会员注册成功通知
	 * 
	 * @param appId  公众号的唯一标识
	 * @param openid 用户的标识，对当前公众号唯一
	 * @param title  会员卡名称
	 * @param name   姓名
	 * @param mobile 手机号
	 * @param bonus  会员积分
	 * @param level  会员等级
	 * @param code   会员卡号
	 * @param url    跳转URL
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> sendMemberRegisterSuccessNotice(String appId, String openid, String title, String name,
			String mobile, int bonus, String level, String code, String url) throws Exception {
		Map<String, Object> postMap = new HashMap<>(16);
		postMap.put("touser", openid);
		postMap.put("template_id", memberRegisterSuccessNoticeId);
		postMap.put("url", url);

		Map<String, Object> dataMap = new HashMap<>(16);

		Map<String, Object> valueMap = new HashMap<>(16);
		valueMap.put("value", memberRegisterSuccessNoticeFirst.replace(TITLE, title));
		dataMap.put("first", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", name);
		dataMap.put("keyword1", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", mobile);
		dataMap.put("keyword2", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", bonus);
		dataMap.put("keyword3", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", level);
		dataMap.put("keyword4", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", code);
		dataMap.put("keyword5", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", memberRegisterSuccessNoticeRemark);
		dataMap.put("remark", valueMap);

		postMap.put("data", dataMap);

		String accessToken = weixinService.getAccessToken(appId);
		return TemplateApi.send(accessToken, postMap);
	}

	/**
	 * 发送会员卡激活成功通知
	 * 
	 * @param appId  公众号的唯一标识
	 * @param openid 用户的标识，对当前公众号唯一
	 * @param title  会员卡名称
	 * @param code   会员卡号
	 * @param time   激活时间
	 * @param url    跳转URL
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> sendMemberCardActivationSuccessNotice(String appId, String openid, String title,
			String code, String time, String url) throws Exception {
		Map<String, Object> postMap = new HashMap<>(16);
		postMap.put("touser", openid);
		postMap.put("template_id", memberCardActivationSuccessNoticeId);
		postMap.put("url", url);

		Map<String, Object> dataMap = new HashMap<>(16);

		Map<String, Object> valueMap = new HashMap<>(16);
		valueMap.put("value", memberCardActivationSuccessNoticeFirst.replace(TITLE, title));
		dataMap.put("first", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", code);
		dataMap.put("keyword1", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", time);
		dataMap.put("keyword2", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", memberCardActivationSuccessNoticeRemark);
		dataMap.put("remark", valueMap);

		postMap.put("data", dataMap);

		String accessToken = weixinService.getAccessToken(appId);
		return TemplateApi.send(accessToken, postMap);
	}
}
