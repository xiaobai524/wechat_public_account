package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券高级信息-商家服务实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardBusinessService implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 微信卡券ID（外键） */
	private String cardId;

	/**
	 * 商家服务类型：BIZ_SERVICE_DELIVER 外卖服务；BIZ_SERVICE_FREE_PARK 停车位；BIZ_SERVICE_WITH_PET 可带宠物；BIZ_SERVICE_FREE_WIFI 免费wifi。
	 */
	private String service;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service == null ? null : service.trim();
	}

	@Override
	public String toString() {
		return "CardBusinessService [id=" + id + ", cardId=" + cardId + ", service=" + service + "]";
	}

}