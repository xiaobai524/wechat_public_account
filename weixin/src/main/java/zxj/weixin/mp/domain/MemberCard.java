package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信会员卡实体类
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCard implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 公众号的唯一标识（外键） */
	private String appId;

	/** 会员卡ID */
	private String cardId;

	/** 员卡自定义卡面背景图 */
	private String backgroundPicUrl;

	/** 特权说明 */
	private String prerogative;

	/** 是否支持积分，仅支持从false变为true，默认为false。 */
	private Boolean supplyBonus;

	/** 积分清零规则 */
	private String bonusCleared;

	/** 积分规则 */
	private String bonusRules;

	/** 积分信息类目跳转的url。 */
	private String bonusUrl;

	/** 是否支持储值，仅支持从false变为true，默认为false。该字段须开通储值功能后方可使用。 */
	private Boolean supplyBalance;

	/** 储值说明 */
	private String balanceRules;

	/** 余额信息类目跳转的url */
	private String balanceUrl;

	/** 是否开通一键开卡 */
	private Boolean wxActivate;

	/** 是否支持跳转型一键激活 */
	private Boolean wxActivateAfterSubmit;

	/** 跳转型一键激活跳转的地址链接 */
	private String wxActivateAfterSubmitUrl;

	/** 是否开通自动激活 */
	private Boolean autoActivate;

	/** 激活会员卡的url */
	private String activateUrl;

	/** 激活会员卡url对应的小程序user_name */
	private String activateAppBrandUserName;

	/** 激活会员卡url对应的小程序path */
	private String activateAppBrandPass;

	/** 折扣，该会员卡享受的折扣优惠 */
	private Integer discount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId == null ? null : appId.trim();
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getBackgroundPicUrl() {
		return backgroundPicUrl;
	}

	public void setBackgroundPicUrl(String backgroundPicUrl) {
		this.backgroundPicUrl = backgroundPicUrl == null ? null : backgroundPicUrl.trim();
	}

	public String getPrerogative() {
		return prerogative;
	}

	public void setPrerogative(String prerogative) {
		this.prerogative = prerogative == null ? null : prerogative.trim();
	}

	public Boolean getSupplyBonus() {
		return supplyBonus;
	}

	public void setSupplyBonus(Boolean supplyBonus) {
		this.supplyBonus = supplyBonus;
	}

	public String getBonusCleared() {
		return bonusCleared;
	}

	public void setBonusCleared(String bonusCleared) {
		this.bonusCleared = bonusCleared == null ? null : bonusCleared.trim();
	}

	public String getBonusRules() {
		return bonusRules;
	}

	public void setBonusRules(String bonusRules) {
		this.bonusRules = bonusRules == null ? null : bonusRules.trim();
	}

	public String getBonusUrl() {
		return bonusUrl;
	}

	public void setBonusUrl(String bonusUrl) {
		this.bonusUrl = bonusUrl == null ? null : bonusUrl.trim();
	}

	public Boolean getSupplyBalance() {
		return supplyBalance;
	}

	public void setSupplyBalance(Boolean supplyBalance) {
		this.supplyBalance = supplyBalance;
	}

	public String getBalanceRules() {
		return balanceRules;
	}

	public void setBalanceRules(String balanceRules) {
		this.balanceRules = balanceRules == null ? null : balanceRules.trim();
	}

	public String getBalanceUrl() {
		return balanceUrl;
	}

	public void setBalanceUrl(String balanceUrl) {
		this.balanceUrl = balanceUrl == null ? null : balanceUrl.trim();
	}

	public Boolean getWxActivate() {
		return wxActivate;
	}

	public void setWxActivate(Boolean wxActivate) {
		this.wxActivate = wxActivate;
	}

	public Boolean getWxActivateAfterSubmit() {
		return wxActivateAfterSubmit;
	}

	public void setWxActivateAfterSubmit(Boolean wxActivateAfterSubmit) {
		this.wxActivateAfterSubmit = wxActivateAfterSubmit;
	}

	public String getWxActivateAfterSubmitUrl() {
		return wxActivateAfterSubmitUrl;
	}

	public void setWxActivateAfterSubmitUrl(String wxActivateAfterSubmitUrl) {
		this.wxActivateAfterSubmitUrl = wxActivateAfterSubmitUrl == null ? null : wxActivateAfterSubmitUrl.trim();
	}

	public Boolean getAutoActivate() {
		return autoActivate;
	}

	public void setAutoActivate(Boolean autoActivate) {
		this.autoActivate = autoActivate;
	}

	public String getActivateUrl() {
		return activateUrl;
	}

	public void setActivateUrl(String activateUrl) {
		this.activateUrl = activateUrl == null ? null : activateUrl.trim();
	}

	public String getActivateAppBrandUserName() {
		return activateAppBrandUserName;
	}

	public void setActivateAppBrandUserName(String activateAppBrandUserName) {
		this.activateAppBrandUserName = activateAppBrandUserName == null ? null : activateAppBrandUserName.trim();
	}

	public String getActivateAppBrandPass() {
		return activateAppBrandPass;
	}

	public void setActivateAppBrandPass(String activateAppBrandPass) {
		this.activateAppBrandPass = activateAppBrandPass == null ? null : activateAppBrandPass.trim();
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "MemberCard [id=" + id + ", appId=" + appId + ", cardId=" + cardId + ", backgroundPicUrl="
				+ backgroundPicUrl + ", prerogative=" + prerogative + ", supplyBonus=" + supplyBonus + ", bonusCleared="
				+ bonusCleared + ", bonusRules=" + bonusRules + ", bonusUrl=" + bonusUrl + ", supplyBalance="
				+ supplyBalance + ", balanceRules=" + balanceRules + ", balanceUrl=" + balanceUrl + ", wxActivate="
				+ wxActivate + ", wxActivateAfterSubmit=" + wxActivateAfterSubmit + ", wxActivateAfterSubmitUrl="
				+ wxActivateAfterSubmitUrl + ", autoActivate=" + autoActivate + ", activateUrl=" + activateUrl
				+ ", activateAppBrandUserName=" + activateAppBrandUserName + ", activateAppBrandPass="
				+ activateAppBrandPass + ", discount=" + discount + "]";
	}

}