package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信门店图片实体类
 * 
 * @author zhuxuejiang
 *
 */
public class BusinessPhoto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 门店ID（外键） */
	private Integer businessId;

	/** 图片URL */
	private String url;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	@Override
	public String toString() {
		return "BusinessPhoto [id=" + id + ", businessId=" + businessId + ", url=" + url + "]";
	}

}