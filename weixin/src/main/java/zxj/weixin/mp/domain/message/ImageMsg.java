package zxj.weixin.mp.domain.message;

/**
 * 微信图片消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class ImageMsg extends BaseNormalMsg {

	/** 图片链接（由系统生成） */
	private String picUrl;

	/** 图片消息媒体id，可以调用多媒体文件下载接口拉取数据 */
	private String mediaId;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "ImageMsg [picUrl=" + picUrl + ", mediaId=" + mediaId + ", msgId=" + msgId + ", toUserName=" + toUserName
				+ ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + "]";
	}

}
