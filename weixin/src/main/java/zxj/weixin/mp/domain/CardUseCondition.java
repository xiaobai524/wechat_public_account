package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券高级信息-使用门槛实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardUseCondition implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 微信卡券ID（主键，外键） */
	private String cardId;

	/** 指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写适用于xxx。 */
	private String acceptCategory;

	/** 指定不可用的商品类目，仅用于代金券类型，填入后将在券面拼写不适用于xxxx。 */
	private String rejectCategory;

	/** 满减门槛字段，可用于兑换券和代金券，填入后将在券面拼写消费满xx元可用。 */
	private Integer leastCost;

	/** 购买xx可用类型门槛，仅用于兑换，填入后自动拼写购买xxx可用。 */
	private String objectUseFor;

	/** 不可以与其他类型共享门槛，填写false时系统将在使用须知里拼写“不可与其他优惠共享”，填写true时系统将在使用须知里拼写“可与其他优惠共享”，默认为true。 */
	private Boolean canUseWithOtherDiscount;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getAcceptCategory() {
		return acceptCategory;
	}

	public void setAcceptCategory(String acceptCategory) {
		this.acceptCategory = acceptCategory == null ? null : acceptCategory.trim();
	}

	public String getRejectCategory() {
		return rejectCategory;
	}

	public void setRejectCategory(String rejectCategory) {
		this.rejectCategory = rejectCategory == null ? null : rejectCategory.trim();
	}

	public Integer getLeastCost() {
		return leastCost;
	}

	public void setLeastCost(Integer leastCost) {
		this.leastCost = leastCost;
	}

	public String getObjectUseFor() {
		return objectUseFor;
	}

	public void setObjectUseFor(String objectUseFor) {
		this.objectUseFor = objectUseFor == null ? null : objectUseFor.trim();
	}

	public Boolean getCanUseWithOtherDiscount() {
		return canUseWithOtherDiscount;
	}

	public void setCanUseWithOtherDiscount(Boolean canUseWithOtherDiscount) {
		this.canUseWithOtherDiscount = canUseWithOtherDiscount;
	}

	@Override
	public String toString() {
		return "CardUseCondition [cardId=" + cardId + ", acceptCategory=" + acceptCategory + ", rejectCategory="
				+ rejectCategory + ", leastCost=" + leastCost + ", objectUseFor=" + objectUseFor
				+ ", canUseWithOtherDiscount=" + canUseWithOtherDiscount + "]";
	}

}