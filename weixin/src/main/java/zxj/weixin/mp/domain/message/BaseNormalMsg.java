package zxj.weixin.mp.domain.message;

/**
 * 微信普通消息基类
 * 
 * @author zhuxuejiang
 *
 */
public abstract class BaseNormalMsg extends BaseMsg {

	/** 消息id，64位整型 */
	protected Long msgId;

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

}
