package zxj.weixin.mp.domain.message;

/**
 * 微信视频消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class VideoMsg extends BaseNormalMsg {

	/** 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。 */
	private String mediaId;

	/** 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。 */
	private String thumbMediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	@Override
	public String toString() {
		return "VideoMsg [mediaId=" + mediaId + ", thumbMediaId=" + thumbMediaId + ", msgId=" + msgId + ", toUserName="
				+ toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType
				+ "]";
	}

}
