package zxj.weixin.mp.domain.message;

/**
 * 微信卡券进入会员卡事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class UserViewCardMsg extends BaseEventMsg {

	/** 卡券ID */
	private String cardId;

	/** 商户自定义code值。非自定code推送为空串。 */
	private String userCardCode;

	/** 商户自定义二维码渠道参数，用于标识本次扫码打开会员卡来源来自于某个渠道值的二维码 */
	private String outerStr;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode;
	}

	public String getOuterStr() {
		return outerStr;
	}

	public void setOuterStr(String outerStr) {
		this.outerStr = outerStr;
	}

}
