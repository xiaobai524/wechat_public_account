package zxj.weixin.mp.domain.message;

/**
 * 微信卡券会员卡激活事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class SubmitMembercardUserInfoMsg extends BaseEventMsg {

	/** 卡券ID */
	private String cardId;

	/** 卡券Code码 */
	private String userCardCode;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode;
	}

}
