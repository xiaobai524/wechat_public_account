package zxj.weixin.mp.domain.vo;

import zxj.weixin.mp.domain.MemberCard;

/**
 * 微信会员卡VO
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCardVO extends MemberCard {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "MemberCardVO [getId()=" + getId() + ", getAppId()=" + getAppId() + ", getCardId()=" + getCardId()
				+ ", getBackgroundPicUrl()=" + getBackgroundPicUrl() + ", getPrerogative()=" + getPrerogative()
				+ ", getSupplyBonus()=" + getSupplyBonus() + ", getBonusCleared()=" + getBonusCleared()
				+ ", getBonusRules()=" + getBonusRules() + ", getBonusUrl()=" + getBonusUrl() + ", getSupplyBalance()="
				+ getSupplyBalance() + ", getBalanceRules()=" + getBalanceRules() + ", getBalanceUrl()="
				+ getBalanceUrl() + ", getWxActivate()=" + getWxActivate() + ", getAutoActivate()=" + getAutoActivate()
				+ ", getActivateUrl()=" + getActivateUrl() + ", getActivateAppBrandUserName()="
				+ getActivateAppBrandUserName() + ", getActivateAppBrandPass()=" + getActivateAppBrandPass()
				+ ", getDiscount()=" + getDiscount() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
