package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券高级信息-封面摘要实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardAbstract implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 微信卡券ID（主键，外键） */
	private String cardId;

	/** 封面摘要简介。 */
	private String summary;

	/** 封面图片。建议图片尺寸像素850*350。 */
	private String iconUrl;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary == null ? null : summary.trim();
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl == null ? null : iconUrl.trim();
	}

	@Override
	public String toString() {
		return "CardAbstract [cardId=" + cardId + ", summary=" + summary + ", iconUrl=" + iconUrl + "]";
	}

}