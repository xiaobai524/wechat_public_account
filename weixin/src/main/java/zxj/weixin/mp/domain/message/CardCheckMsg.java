package zxj.weixin.mp.domain.message;

/**
 * 微信卡券审核事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class CardCheckMsg extends BaseEventMsg {

	/** 卡券ID */
	private String cardId;

	/** 审核不通过原因 */
	private String refuseReason;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

}
