package zxj.weixin.mp.domain.message;

/**
 * 微信普通消息基类
 * 
 * @author zhuxuejiang
 *
 */
public class PoiCheckNotifyEventMsg extends BaseMsg {

	/** 商户自己内部ID，即字段中的sid */
	protected String uniqId;

	/** 微信的门店ID，微信内门店唯一标示ID */
	protected String poiId;

	/** 审核结果，成功succ 或失败fail */
	protected String result;

	/** 成功的通知信息，或审核失败的驳回理由 */
	protected String msg;

	public String getUniqId() {
		return uniqId;
	}

	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}

	public String getPoiId() {
		return poiId;
	}

	public void setPoiId(String poiId) {
		this.poiId = poiId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
