package zxj.weixin.mp.domain.message;

/**
 * 微信关注/取消关注事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class SubscribeEventMsg extends BaseEventMsg {

	/** 事件KEY值，qrscene_为前缀，后面为二维码的参数值 */
	private String eventKey;

	/** 二维码的ticket，可用来换取二维码图片 */
	private String ticket;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
