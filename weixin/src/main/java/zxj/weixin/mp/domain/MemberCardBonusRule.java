package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信会员卡积分规则实体类
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCardBonusRule implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键（与wx_mp_member_card相同） */
	private Integer id;

	/** 消费金额，以分为单位 */
	private Integer costMoneyUnit;

	/** 根据以上消费金额对应增加的积分 */
	private Integer increaseBonus;

	/** 单次获取的积分上限 */
	private Integer maxIncreaseBonus;

	/** 用户激活后获得的初始积分 */
	private Integer initIncreaseBonus;

	/** 每使用x积分。 */
	private Integer costBonusUnit;

	/** 抵扣xx元，（这里以分为单位） */
	private Integer reduceMoney;

	/** 抵扣条件，满xx元（这里以分为单位）可用 */
	private Integer leastMoneyToUseBonus;

	/** 抵扣条件，单笔最多使用xx积分 */
	private Integer maxReduceBonus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCostMoneyUnit() {
		return costMoneyUnit;
	}

	public void setCostMoneyUnit(Integer costMoneyUnit) {
		this.costMoneyUnit = costMoneyUnit;
	}

	public Integer getIncreaseBonus() {
		return increaseBonus;
	}

	public void setIncreaseBonus(Integer increaseBonus) {
		this.increaseBonus = increaseBonus;
	}

	public Integer getMaxIncreaseBonus() {
		return maxIncreaseBonus;
	}

	public void setMaxIncreaseBonus(Integer maxIncreaseBonus) {
		this.maxIncreaseBonus = maxIncreaseBonus;
	}

	public Integer getInitIncreaseBonus() {
		return initIncreaseBonus;
	}

	public void setInitIncreaseBonus(Integer initIncreaseBonus) {
		this.initIncreaseBonus = initIncreaseBonus;
	}

	public Integer getCostBonusUnit() {
		return costBonusUnit;
	}

	public void setCostBonusUnit(Integer costBonusUnit) {
		this.costBonusUnit = costBonusUnit;
	}

	public Integer getReduceMoney() {
		return reduceMoney;
	}

	public void setReduceMoney(Integer reduceMoney) {
		this.reduceMoney = reduceMoney;
	}

	public Integer getLeastMoneyToUseBonus() {
		return leastMoneyToUseBonus;
	}

	public void setLeastMoneyToUseBonus(Integer leastMoneyToUseBonus) {
		this.leastMoneyToUseBonus = leastMoneyToUseBonus;
	}

	public Integer getMaxReduceBonus() {
		return maxReduceBonus;
	}

	public void setMaxReduceBonus(Integer maxReduceBonus) {
		this.maxReduceBonus = maxReduceBonus;
	}

	@Override
	public String toString() {
		return "MemberCardBonusRule [id=" + id + ", costMoneyUnit=" + costMoneyUnit + ", increaseBonus=" + increaseBonus
				+ ", maxIncreaseBonus=" + maxIncreaseBonus + ", initIncreaseBonus=" + initIncreaseBonus
				+ ", costBonusUnit=" + costBonusUnit + ", reduceMoney=" + reduceMoney + ", leastMoneyToUseBonus="
				+ leastMoneyToUseBonus + ", maxReduceBonus=" + maxReduceBonus + "]";
	}

}