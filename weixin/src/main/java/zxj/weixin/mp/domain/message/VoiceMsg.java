package zxj.weixin.mp.domain.message;

/**
 * 微信语音消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class VoiceMsg extends BaseNormalMsg {

	/** 语音消息媒体id，可以调用多媒体文件下载接口拉取数据 */
	private String mediaId;

	/** 语音格式，如amr，speex等 */
	private String format;

	/** 语音识别结果，UTF8编码 */
	private String recognition;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getRecognition() {
		return recognition;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	@Override
	public String toString() {
		return "VoiceMsg [mediaId=" + mediaId + ", format=" + format + ", recognition=" + recognition + ", msgId="
				+ msgId + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + "]";
	}

}
