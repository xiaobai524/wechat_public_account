package zxj.weixin.mp.domain.message;

/**
 * 微信卡券领取事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class UserDelCardMsg extends BaseEventMsg {

	/** 卡券ID */
	private String cardId;

	/** code序列号。自定义code及非自定义code的卡券被领取后都支持事件推送。 */
	private String userCardCode;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode;
	}

}
