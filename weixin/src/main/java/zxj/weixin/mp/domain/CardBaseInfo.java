package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券基础信息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardBaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 微信卡券ID（主键，外键） */
	private String cardId;

	/** 公众号的唯一标识（外键） */
	private String appId;

	/** 卡券类型：MEMBER_CARD-会员卡。 */
	private String cardType;

	/** 审核状态：card_checking-卡券审核中；card_pass_check-卡券通过审核；card_not_pass_check-卡券未通过审核。 */
	private String checkState;

	/** 审核不通过原因。 */
	private String refuseReason;

	/** 卡券的商户logo，建议像素为300*300。 */
	private String logoUrl;

	/**
	 * 码型："CODE_TYPE_TEXT"文本；"CODE_TYPE_BARCODE"一维码；"CODE_TYPE_QRCODE"二维码；"CODE_TYPE_ONLY_QRCODE"二维码无code显示；"CODE_TYPE_ONLY_BARCODE"一维码无code显示；"CODE_TYPE_NONE"不显示code和条形码类型。
	 */
	private String codeType;

	/** 商户名字，字数上限为12个汉字。 */
	private String brandName;

	/** 卡券名，字数上限为9个汉字。 */
	private String title;

	/** 券颜色。按色彩规范标注填写Color010-Color100。 */
	private String color;

	/** 卡券使用提醒，字数上限为16个汉字。 */
	private String notice;

	/** 卡券使用说明，字数上限为1024个汉字。 */
	private String description;

	/** 卡券库存的数量，上限为100000000。 */
	private Integer skuQuantity;

	/** 使用时间的类型。DATE_TYPE_FIX _TIME_RANGE 表示固定日期区间，DATETYPE FIX_TERM 表示固定时长（自领取后按天算）。 */
	private String dateInfoType;

	/** type为DATE_TYPE_FIX_TIME_RANGE时专用，表示起用时间。从1970年1月1日00:00:00至起用时间的秒数，最终需转换为字符串形态传入。（东八区时间,UTC+8，单位为秒） */
	private Long beginTimestamp;

	/** 表示结束时间，建议设置为截止日期的23:59:59过期。（东八区时间，UTC+8，单位为秒） */
	private Long endTimestamp;

	/** type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天内有效，不支持填写0。 */
	private Integer fixedTerm;

	/** type为DATE_TYPE_FIX_TERM时专用，表示自领取后多少天开始生效，领取后当天生效填写0。（单位为天） */
	private Integer fixedBeginTerm;

	/** 是否自定义Code码。 */
	private Boolean useCustomCode;

	/** 填入GET_CUSTOM_CODE_MODE_DEPOSIT表示该卡券为预存code模式卡券，须导入超过库存数目的自定义code后方可投放，填入该字段后，quantity字段须为0，须导入code后再增加库存。 */
	private String getCustomCodeMode;

	/** 是否指定用户领取，填写true或false。默认为false。通常指定特殊用户群体投放卡券或防止刷券时选择指定用户领取。 */
	private Boolean bindOpenid;

	/** 客服电话。 */
	private String servicePhone;

	/** 设置本卡券支持全部门店，与location_id_list互斥。 */
	private Boolean useAllLocations;

	/** 卡券顶部居中的按钮，仅在卡券状态正常(可以核销)时显示。 */
	private String centerTitle;

	/** 显示在入口下方的提示语，仅在卡券状态正常(可以核销)时显示。 */
	private String centerSubTitle;

	/** 顶部居中的url，仅在卡券状态正常(可以核销)时显示。 */
	private String centerUrl;

	/** 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序。 */
	private String centerAppBrandUserName;

	/** 卡券跳转的小程序的path。 */
	private String centerAppBrandPass;

	/** 自定义跳转外链的入口名字。 */
	private String customUrlName;

	/** 自定义跳转的URL。 */
	private String customUrl;

	/** 显示在入口右侧的提示语。 */
	private String customUrlSubTitle;

	/** 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序 。 */
	private String customAppBrandUserName;

	/** 卡券跳转的小程序的path。 */
	private String customAppBrandPass;

	/** 营销场景的自定义入口名称。 */
	private String promotionUrlName;

	/** 入口跳转外链的地址链接。 */
	private String promotionUrl;

	/** 显示在营销入口右侧的提示语。 */
	private String promotionUrlSubTitle;

	/** 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序。 */
	private String promotionAppBrandUserName;

	/** 卡券跳转的小程序的path */
	private String promotionAppBrandPass;

	/** 每人可领券的数量限制,不填写默认为50。 */
	private Integer getLimit;

	/** 每人可核销的数量限制,不填写默认为50。 */
	private Integer useLimit;

	/** 卡券领取页面是否可分享。 */
	private Boolean canShare;

	/** 卡券是否可转赠。 */
	private Boolean canGiveFriend;

	/** 是否设置该会员卡支持拉出微信支付刷卡界面（会员卡独有） */
	private Boolean isSwipeCard;

	/** 是否设置该会员卡中部的按钮同时支持微信支付刷卡和会员卡二维码（会员卡独有） */
	private Boolean isPayAndQrcode;

	/** 填写true为用户点击进入会员卡时推送事件，默认为false。详情见进入会员卡事件推送（会员卡独有） */
	private Boolean needPushOnView;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId == null ? null : appId.trim();
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType == null ? null : cardType.trim();
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState == null ? null : checkState.trim();
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason == null ? null : refuseReason.trim();
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl == null ? null : logoUrl.trim();
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType == null ? null : codeType.trim();
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName == null ? null : brandName.trim();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color == null ? null : color.trim();
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice == null ? null : notice.trim();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? null : description.trim();
	}

	public Integer getSkuQuantity() {
		return skuQuantity;
	}

	public void setSkuQuantity(Integer skuQuantity) {
		this.skuQuantity = skuQuantity;
	}

	public String getDateInfoType() {
		return dateInfoType;
	}

	public void setDateInfoType(String dateInfoType) {
		this.dateInfoType = dateInfoType == null ? null : dateInfoType.trim();
	}

	public Long getBeginTimestamp() {
		return beginTimestamp;
	}

	public void setBeginTimestamp(Long beginTimestamp) {
		this.beginTimestamp = beginTimestamp;
	}

	public Long getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(Long endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

	public Integer getFixedTerm() {
		return fixedTerm;
	}

	public void setFixedTerm(Integer fixedTerm) {
		this.fixedTerm = fixedTerm;
	}

	public Integer getFixedBeginTerm() {
		return fixedBeginTerm;
	}

	public void setFixedBeginTerm(Integer fixedBeginTerm) {
		this.fixedBeginTerm = fixedBeginTerm;
	}

	public Boolean getUseCustomCode() {
		return useCustomCode;
	}

	public void setUseCustomCode(Boolean useCustomCode) {
		this.useCustomCode = useCustomCode;
	}

	public String getGetCustomCodeMode() {
		return getCustomCodeMode;
	}

	public void setGetCustomCodeMode(String getCustomCodeMode) {
		this.getCustomCodeMode = getCustomCodeMode == null ? null : getCustomCodeMode.trim();
	}

	public Boolean getBindOpenid() {
		return bindOpenid;
	}

	public void setBindOpenid(Boolean bindOpenid) {
		this.bindOpenid = bindOpenid;
	}

	public String getServicePhone() {
		return servicePhone;
	}

	public void setServicePhone(String servicePhone) {
		this.servicePhone = servicePhone == null ? null : servicePhone.trim();
	}

	public Boolean getUseAllLocations() {
		return useAllLocations;
	}

	public void setUseAllLocations(Boolean useAllLocations) {
		this.useAllLocations = useAllLocations;
	}

	public String getCenterTitle() {
		return centerTitle;
	}

	public void setCenterTitle(String centerTitle) {
		this.centerTitle = centerTitle == null ? null : centerTitle.trim();
	}

	public String getCenterSubTitle() {
		return centerSubTitle;
	}

	public void setCenterSubTitle(String centerSubTitle) {
		this.centerSubTitle = centerSubTitle == null ? null : centerSubTitle.trim();
	}

	public String getCenterUrl() {
		return centerUrl;
	}

	public void setCenterUrl(String centerUrl) {
		this.centerUrl = centerUrl == null ? null : centerUrl.trim();
	}

	public String getCenterAppBrandUserName() {
		return centerAppBrandUserName;
	}

	public void setCenterAppBrandUserName(String centerAppBrandUserName) {
		this.centerAppBrandUserName = centerAppBrandUserName == null ? null : centerAppBrandUserName.trim();
	}

	public String getCenterAppBrandPass() {
		return centerAppBrandPass;
	}

	public void setCenterAppBrandPass(String centerAppBrandPass) {
		this.centerAppBrandPass = centerAppBrandPass == null ? null : centerAppBrandPass.trim();
	}

	public String getCustomUrlName() {
		return customUrlName;
	}

	public void setCustomUrlName(String customUrlName) {
		this.customUrlName = customUrlName == null ? null : customUrlName.trim();
	}

	public String getCustomUrl() {
		return customUrl;
	}

	public void setCustomUrl(String customUrl) {
		this.customUrl = customUrl == null ? null : customUrl.trim();
	}

	public String getCustomUrlSubTitle() {
		return customUrlSubTitle;
	}

	public void setCustomUrlSubTitle(String customUrlSubTitle) {
		this.customUrlSubTitle = customUrlSubTitle == null ? null : customUrlSubTitle.trim();
	}

	public String getCustomAppBrandUserName() {
		return customAppBrandUserName;
	}

	public void setCustomAppBrandUserName(String customAppBrandUserName) {
		this.customAppBrandUserName = customAppBrandUserName == null ? null : customAppBrandUserName.trim();
	}

	public String getCustomAppBrandPass() {
		return customAppBrandPass;
	}

	public void setCustomAppBrandPass(String customAppBrandPass) {
		this.customAppBrandPass = customAppBrandPass == null ? null : customAppBrandPass.trim();
	}

	public String getPromotionUrlName() {
		return promotionUrlName;
	}

	public void setPromotionUrlName(String promotionUrlName) {
		this.promotionUrlName = promotionUrlName == null ? null : promotionUrlName.trim();
	}

	public String getPromotionUrl() {
		return promotionUrl;
	}

	public void setPromotionUrl(String promotionUrl) {
		this.promotionUrl = promotionUrl == null ? null : promotionUrl.trim();
	}

	public String getPromotionUrlSubTitle() {
		return promotionUrlSubTitle;
	}

	public void setPromotionUrlSubTitle(String promotionUrlSubTitle) {
		this.promotionUrlSubTitle = promotionUrlSubTitle == null ? null : promotionUrlSubTitle.trim();
	}

	public String getPromotionAppBrandUserName() {
		return promotionAppBrandUserName;
	}

	public void setPromotionAppBrandUserName(String promotionAppBrandUserName) {
		this.promotionAppBrandUserName = promotionAppBrandUserName == null ? null : promotionAppBrandUserName.trim();
	}

	public String getPromotionAppBrandPass() {
		return promotionAppBrandPass;
	}

	public void setPromotionAppBrandPass(String promotionAppBrandPass) {
		this.promotionAppBrandPass = promotionAppBrandPass == null ? null : promotionAppBrandPass.trim();
	}

	public Integer getGetLimit() {
		return getLimit;
	}

	public void setGetLimit(Integer getLimit) {
		this.getLimit = getLimit;
	}

	public Integer getUseLimit() {
		return useLimit;
	}

	public void setUseLimit(Integer useLimit) {
		this.useLimit = useLimit;
	}

	public Boolean getCanShare() {
		return canShare;
	}

	public void setCanShare(Boolean canShare) {
		this.canShare = canShare;
	}

	public Boolean getCanGiveFriend() {
		return canGiveFriend;
	}

	public void setCanGiveFriend(Boolean canGiveFriend) {
		this.canGiveFriend = canGiveFriend;
	}

	public Boolean getIsSwipeCard() {
		return isSwipeCard;
	}

	public void setIsSwipeCard(Boolean isSwipeCard) {
		this.isSwipeCard = isSwipeCard;
	}

	public Boolean getIsPayAndQrcode() {
		return isPayAndQrcode;
	}

	public void setIsPayAndQrcode(Boolean isPayAndQrcode) {
		this.isPayAndQrcode = isPayAndQrcode;
	}

	public Boolean getNeedPushOnView() {
		return needPushOnView;
	}

	public void setNeedPushOnView(Boolean needPushOnView) {
		this.needPushOnView = needPushOnView;
	}

	@Override
	public String toString() {
		return "CardBaseInfo [cardId=" + cardId + ", appId=" + appId + ", cardType=" + cardType + ", checkState="
				+ checkState + ", refuseReason=" + refuseReason + ", logoUrl=" + logoUrl + ", codeType=" + codeType
				+ ", brandName=" + brandName + ", title=" + title + ", color=" + color + ", notice=" + notice
				+ ", description=" + description + ", skuQuantity=" + skuQuantity + ", dateInfoType=" + dateInfoType
				+ ", beginTimestamp=" + beginTimestamp + ", endTimestamp=" + endTimestamp + ", fixedTerm=" + fixedTerm
				+ ", fixedBeginTerm=" + fixedBeginTerm + ", useCustomCode=" + useCustomCode + ", getCustomCodeMode="
				+ getCustomCodeMode + ", bindOpenid=" + bindOpenid + ", servicePhone=" + servicePhone
				+ ", useAllLocations=" + useAllLocations + ", centerTitle=" + centerTitle + ", centerSubTitle="
				+ centerSubTitle + ", centerUrl=" + centerUrl + ", centerAppBrandUserName=" + centerAppBrandUserName
				+ ", centerAppBrandPass=" + centerAppBrandPass + ", customUrlName=" + customUrlName + ", customUrl="
				+ customUrl + ", customUrlSubTitle=" + customUrlSubTitle + ", customAppBrandUserName="
				+ customAppBrandUserName + ", customAppBrandPass=" + customAppBrandPass + ", promotionUrlName="
				+ promotionUrlName + ", promotionUrl=" + promotionUrl + ", promotionUrlSubTitle=" + promotionUrlSubTitle
				+ ", promotionAppBrandUserName=" + promotionAppBrandUserName + ", promotionAppBrandPass="
				+ promotionAppBrandPass + ", getLimit=" + getLimit + ", useLimit=" + useLimit + ", canShare=" + canShare
				+ ", canGiveFriend=" + canGiveFriend + ", isSwipeCard=" + isSwipeCard + ", isPayAndQrcode="
				+ isPayAndQrcode + ", needPushOnView=" + needPushOnView + "]";
	}

}