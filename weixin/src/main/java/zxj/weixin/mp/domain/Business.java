package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信门店实体类
 * 
 * @author zhuxuejiang
 *
 */
public class Business implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键。商户自己的id，用于后续审核通过收到poi_id的通知时，做对应关系。请商户自己保证唯一识别性 */
	private Integer id;

	/** 公众号的唯一标识（外键） */
	private String appId;

	/** 微信的门店ID微信内门店唯一标示ID */
	private String poiId;

	/** 门店名称（仅为商户名，如：国美、麦当劳，不应包含地区、地址、分店名等信息，错误示例：北京国美） 不能为空，15个汉字或30个英文字符内 */
	private String businessName;

	/** 分店名称（不应包含地区信息，不应与门店名有重复，错误示例：北京王府井店） 20个字以内 */
	private String branchName;

	/** 门店所在的省份（直辖市填城市名,如：北京市） 10个字以内 */
	private String province;

	/** 门店所在的城市 10个字以内 */
	private String city;

	/** 门店所在地区 10个字以内 */
	private String district;

	/** 门店所在的详细街道地址（不要填写省市信息） */
	private String address;

	/** 门店的电话（纯数字，区号、分机号均由“-”隔开） */
	private String telephone;

	/** 坐标类型： 1 为火星坐标 2 为sogou经纬度 3 为百度经纬度 4 为mapbar经纬度 5 为GPS坐标 6 为sogou墨卡托坐标 注：高德经纬度无需转换可直接使用 */
	private Byte offsetType;

	/** 门店所在地理位置的经度 */
	private Double longitude;

	/** 门店所在地理位置的纬度（经纬度均为火星坐标，最好选用腾讯地图标记的坐标） */
	private Double latitude;

	/** 推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容 200字以内 */
	private String recommend;

	/** 特色服务，如免费wifi，免费停车，送货上门等商户能提供的特色功能或服务 */
	private String special;

	/** 商户简介，主要介绍商户信息等 300字以内 */
	private String introduction;

	/** 营业时间，24小时制表示，用“-”连接，如 8:00-20:00 */
	private String openTime;

	/** 人均价格，大于0的整数 */
	private Integer avgPrice;

	/** 审核结果，成功succ或失败fail */
	private String poiCheckNotifyResult;

	/** 成功的通知信息，或审核失败的驳回理由 */
	private String poiCheckNotifyMsg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId == null ? null : appId.trim();
	}

	public String getPoiId() {
		return poiId;
	}

	public void setPoiId(String poiId) {
		this.poiId = poiId == null ? null : poiId.trim();
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName == null ? null : businessName.trim();
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName == null ? null : branchName.trim();
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province == null ? null : province.trim();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district == null ? null : district.trim();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone == null ? null : telephone.trim();
	}

	public Byte getOffsetType() {
		return offsetType;
	}

	public void setOffsetType(Byte offsetType) {
		this.offsetType = offsetType;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getRecommend() {
		return recommend;
	}

	public void setRecommend(String recommend) {
		this.recommend = recommend == null ? null : recommend.trim();
	}

	public String getSpecial() {
		return special;
	}

	public void setSpecial(String special) {
		this.special = special == null ? null : special.trim();
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction == null ? null : introduction.trim();
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime == null ? null : openTime.trim();
	}

	public Integer getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(Integer avgPrice) {
		this.avgPrice = avgPrice;
	}

	public String getPoiCheckNotifyResult() {
		return poiCheckNotifyResult;
	}

	public void setPoiCheckNotifyResult(String poiCheckNotifyResult) {
		this.poiCheckNotifyResult = poiCheckNotifyResult == null ? null : poiCheckNotifyResult.trim();
	}

	public String getPoiCheckNotifyMsg() {
		return poiCheckNotifyMsg;
	}

	public void setPoiCheckNotifyMsg(String poiCheckNotifyMsg) {
		this.poiCheckNotifyMsg = poiCheckNotifyMsg == null ? null : poiCheckNotifyMsg.trim();
	}

	@Override
	public String toString() {
		return "Business [id=" + id + ", appId=" + appId + ", poiId=" + poiId + ", businessName=" + businessName
				+ ", branchName=" + branchName + ", province=" + province + ", city=" + city + ", district=" + district
				+ ", address=" + address + ", telephone=" + telephone + ", offsetType=" + offsetType + ", longitude="
				+ longitude + ", latitude=" + latitude + ", recommend=" + recommend + ", special=" + special
				+ ", introduction=" + introduction + ", openTime=" + openTime + ", avgPrice=" + avgPrice
				+ ", poiCheckNotifyResult=" + poiCheckNotifyResult + ", poiCheckNotifyMsg=" + poiCheckNotifyMsg + "]";
	}

}
