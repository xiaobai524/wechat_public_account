package zxj.weixin.mp.domain.message;

/**
 * 微信文本消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class TextMsg extends BaseNormalMsg {

	/** 文本消息内容 */
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TextMsg [content=" + content + ", msgId=" + msgId + ", toUserName=" + toUserName + ", fromUserName="
				+ fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + "]";
	}

}
