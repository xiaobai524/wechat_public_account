package zxj.weixin.mp.domain.message;

/**
 * 微信地理位置消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class LocationMsg extends BaseNormalMsg {

	/** 地理位置纬度 */
	private String locationX;

	/** 地理位置经度 */
	private String locationY;

	/** 地图缩放大小 */
	private String scale;

	/** 地理位置信息 */
	private String label;

	public String getLocationX() {
		return locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "LocationMsg [locationX=" + locationX + ", locationY=" + locationY + ", scale=" + scale + ", label="
				+ label + ", msgId=" + msgId + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + "]";
	}

}
