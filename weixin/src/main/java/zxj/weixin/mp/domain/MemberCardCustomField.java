package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信会员卡自定义信息类目实体类
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCardCustomField implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 微信会员卡ID（外键） */
	private Integer memberCardId;

	/** 会员信息类目半自定义名称，当开发者变更这类类目信息的value值时可以选择触发系统模板消息通知用户。 */
	private String nameType;

	/** 会员信息类目自定义名称，当开发者变更这类类目信息的value值时不会触发系统模板消息通知用户。 */
	private String name;

	/** 点击类目跳转外链url。 */
	private String url;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMemberCardId() {
		return memberCardId;
	}

	public void setMemberCardId(Integer memberCardId) {
		this.memberCardId = memberCardId;
	}

	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType == null ? null : nameType.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	@Override
	public String toString() {
		return "MemberCardCustomField [id=" + id + ", memberCardId=" + memberCardId + ", nameType=" + nameType
				+ ", name=" + name + ", url=" + url + "]";
	}

}