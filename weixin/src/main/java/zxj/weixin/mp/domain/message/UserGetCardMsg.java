package zxj.weixin.mp.domain.message;

/**
 * 微信卡券领取事件推送消息
 * 
 * @author zhuxuejiang
 *
 */
public class UserGetCardMsg extends BaseEventMsg {

	/** 卡券ID */
	private String cardId;

	/** 是否为转赠领取，1代表是，0代表否。 */
	private Integer isGiveByFriend;

	/** 当IsGiveByFriend为1时填入的字段，表示发起转赠用户的openid */
	private String friendUserName;

	/** code序列号。 */
	private String userCardCode;

	/** 为保证安全，微信会在转赠发生后变更该卡券的code号，该字段表示转赠前的code。 */
	private String oldUserCardCode;

	/** 领取场景值，用于领取渠道数据统计。可在生成二维码接口及添加Addcard接口中自定义该字段的字符串值。 */
	private String outerStr;

	/** 用户删除会员卡后可重新找回，当用户本次操作为找回时，该值为1，否则为0 */
	private Integer isRestoreMemberCard;

	private Integer outerId;

	private Integer isRecommendByFriend;

	private String sourceScene;

	/** 领券用户的UnionId */
	private String unionId;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Integer getIsGiveByFriend() {
		return isGiveByFriend;
	}

	public void setIsGiveByFriend(Integer isGiveByFriend) {
		this.isGiveByFriend = isGiveByFriend;
	}

	public String getFriendUserName() {
		return friendUserName;
	}

	public void setFriendUserName(String friendUserName) {
		this.friendUserName = friendUserName;
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode;
	}

	public String getOldUserCardCode() {
		return oldUserCardCode;
	}

	public void setOldUserCardCode(String oldUserCardCode) {
		this.oldUserCardCode = oldUserCardCode;
	}

	public String getOuterStr() {
		return outerStr;
	}

	public void setOuterStr(String outerStr) {
		this.outerStr = outerStr;
	}

	public Integer getIsRestoreMemberCard() {
		return isRestoreMemberCard;
	}

	public void setIsRestoreMemberCard(Integer isRestoreMemberCard) {
		this.isRestoreMemberCard = isRestoreMemberCard;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public Integer getOuterId() {
		return outerId;
	}

	public void setOuterId(Integer outerId) {
		this.outerId = outerId;
	}

	public Integer getIsRecommendByFriend() {
		return isRecommendByFriend;
	}

	public void setIsRecommendByFriend(Integer isRecommendByFriend) {
		this.isRecommendByFriend = isRecommendByFriend;
	}

	public String getSourceScene() {
		return sourceScene;
	}

	public void setSourceScene(String sourceScene) {
		this.sourceScene = sourceScene;
	}

}
