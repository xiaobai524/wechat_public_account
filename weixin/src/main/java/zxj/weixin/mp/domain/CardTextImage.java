package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券高级信息-图文实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardTextImage implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 微信卡券ID（外键） */
	private String cardId;

	/** 图文描述。 */
	private String text;

	/** 图片链接。 */
	private String imageUrl;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text == null ? null : text.trim();
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl == null ? null : imageUrl.trim();
	}

	@Override
	public String toString() {
		return "CardTextImage [id=" + id + ", cardId=" + cardId + ", text=" + text + ", imageUrl=" + imageUrl + "]";
	}

}