package zxj.weixin.mp.domain.message;

/**
 * 微信事件推送消息基类
 * 
 * @author zhuxuejiang
 *
 */
public abstract class BaseEventMsg extends BaseMsg {

	/** 事件类型 */
	protected String event;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

}
