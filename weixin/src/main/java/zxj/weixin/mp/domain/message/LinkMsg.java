package zxj.weixin.mp.domain.message;

/**
 * 微信链接消息实体类
 * 
 * @author zhuxuejiang
 *
 */
public class LinkMsg extends BaseNormalMsg {

	/** 消息标题 */
	private String title;

	/** 消息描述 */
	private String description;

	/** 消息链接 */
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "LinkMsg [title=" + title + ", description=" + description + ", url=" + url + ", msgId=" + msgId
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + "]";
	}

}
