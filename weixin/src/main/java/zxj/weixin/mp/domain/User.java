package zxj.weixin.mp.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信公众号用户实体类
 * 
 * @author zhuxuejiang
 *
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String appId;

	private Byte subscribe;

	private Date subscribeTime;

	private Date unsubscribeTime;

	private String openid;

	private String nickname;

	private Byte sex;

	private String country;

	private String province;

	private String city;

	private String language;

	private String headimgurl;

	private String unionid;

	private String remark;

	private String privilege;

	private Integer groupid;

	private String subscribeScene;

	private String qrScene;

	private String qrSceneStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId == null ? null : appId.trim();
	}

	public Byte getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Byte subscribe) {
		this.subscribe = subscribe;
	}

	public Date getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public Date getUnsubscribeTime() {
		return unsubscribeTime;
	}

	public void setUnsubscribeTime(Date unsubscribeTime) {
		this.unsubscribeTime = unsubscribeTime;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid == null ? null : openid.trim();
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname == null ? null : nickname.trim();
	}

	public Byte getSex() {
		return sex;
	}

	public void setSex(Byte sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country == null ? null : country.trim();
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province == null ? null : province.trim();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? null : language.trim();
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl == null ? null : headimgurl.trim();
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid == null ? null : unionid.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public String getPrivilege() {
		return privilege;
	}

	public void setPrivilege(String privilege) {
		this.privilege = privilege == null ? null : privilege.trim();
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public String getSubscribeScene() {
		return subscribeScene;
	}

	public void setSubscribeScene(String subscribeScene) {
		this.subscribeScene = subscribeScene == null ? null : subscribeScene.trim();
	}

	public String getQrScene() {
		return qrScene;
	}

	public void setQrScene(String qrScene) {
		this.qrScene = qrScene == null ? null : qrScene.trim();
	}

	public String getQrSceneStr() {
		return qrSceneStr;
	}

	public void setQrSceneStr(String qrSceneStr) {
		this.qrSceneStr = qrSceneStr == null ? null : qrSceneStr.trim();
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", appId=" + appId + ", subscribe=" + subscribe + ", subscribeTime=" + subscribeTime
				+ ", unsubscribeTime=" + unsubscribeTime + ", openid=" + openid + ", nickname=" + nickname + ", sex="
				+ sex + ", country=" + country + ", province=" + province + ", city=" + city + ", language=" + language
				+ ", headimgurl=" + headimgurl + ", unionid=" + unionid + ", remark=" + remark + ", privilege="
				+ privilege + ", groupid=" + groupid + ", subscribeScene=" + subscribeScene + ", qrScene=" + qrScene
				+ ", qrSceneStr=" + qrSceneStr + "]";
	}

}