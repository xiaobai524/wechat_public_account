package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信会员卡自定义信息类目实体类
 * 
 * @author zhuxuejiang
 *
 */
public class MemberCardCellField implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 微信会员卡ID（外键） */
	private Integer memberCardId;

	/** 入口名称。 */
	private String name;

	/** 入口右侧提示语，6个汉字内。 */
	private String tips;

	/** 入口跳转链接。 */
	private String url;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMemberCardId() {
		return memberCardId;
	}

	public void setMemberCardId(Integer memberCardId) {
		this.memberCardId = memberCardId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips == null ? null : tips.trim();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	@Override
	public String toString() {
		return "MemberCardCellField [id=" + id + ", memberCardId=" + memberCardId + ", name=" + name + ", tips=" + tips
				+ ", url=" + url + "]";
	}

}