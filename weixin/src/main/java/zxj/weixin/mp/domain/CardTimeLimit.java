package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信卡券高级信息-使用时段限制实体类
 * 
 * @author zhuxuejiang
 *
 */
public class CardTimeLimit implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 微信卡券ID（外键） */
	private String cardId;

	/**
	 * 限制类型枚举值。支持填入：MONDAY 周一；TUESDAY 周二；WEDNESDAY 周三；THURSDAY 周四；FRIDAY 周五；SATURDAY 周六；SUNDAY
	 * 周日。此处只控制显示，不控制实际使用逻辑，不填默认不显示。
	 */
	private String type;

	/** 当前type类型下的起始时间（小时），如当前结构体内填写了MONDAY，此处填写了10，则此处表示周一 10:00可用。 */
	private Short beginHour;

	/** 当前type类型下的起始时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59可用。 */
	private Short beginMinute;

	/** 当前type类型下的结束时间（小时），如当前结构体内填写了MONDAY，此处填写了20，则此处表示周一 10:00-20:00可用。 */
	private Short endHour;

	/** 当前type类型下的结束时间（分钟），如当前结构体内填写了MONDAY，begin_hour填写10，此处填写了59，则此处表示周一 10:59-20:59可用。 */
	private Short endMinute;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public Short getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(Short beginHour) {
		this.beginHour = beginHour;
	}

	public Short getBeginMinute() {
		return beginMinute;
	}

	public void setBeginMinute(Short beginMinute) {
		this.beginMinute = beginMinute;
	}

	public Short getEndHour() {
		return endHour;
	}

	public void setEndHour(Short endHour) {
		this.endHour = endHour;
	}

	public Short getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(Short endMinute) {
		this.endMinute = endMinute;
	}

	@Override
	public String toString() {
		return "CardTimeLimit [id=" + id + ", cardId=" + cardId + ", type=" + type + ", beginHour=" + beginHour
				+ ", beginMinute=" + beginMinute + ", endHour=" + endHour + ", endMinute=" + endMinute + "]";
	}

}