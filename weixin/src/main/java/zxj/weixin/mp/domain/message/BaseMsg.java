package zxj.weixin.mp.domain.message;

/**
 * 微信消息基类
 * 
 * @author zhuxuejiang
 *
 */
public abstract class BaseMsg {

	/** 开发者微信号 */
	protected String toUserName;

	/** 发送方帐号（一个OpenID） */
	protected String fromUserName;

	/** 消息创建时间 （整型） */
	protected Long createTime;

	/** 消息类型 */
	protected String msgType;

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
