package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信公众号用户领取的会员卡实体类
 * 
 * @author zhuxuejiang
 *
 */
public class UserCard implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Integer id;

	/** 公众号的唯一标识（外键） */
	private String appId;

	/** 微信原始ID */
	private String wxId;

	/** 用户的标识，对当前公众号唯一 */
	private String openid;

	/** 卡券ID */
	private String cardId;

	/** 卡券状态。normal-正常；delete-删除。 */
	private String cardState;

	/** 是否已经被激活 */
	private Boolean hasActive;

	/** 是否为转赠领取，1代表是，0代表否。 */
	private Boolean isGiveByFriend;

	/** 当IsGiveByFriend为1时填入的字段，表示发起转赠用户的openid */
	private String friendUserName;

	/** code序列号。 */
	private String userCardCode;

	/** 为保证安全，微信会在转赠发生后变更该卡券的code号，该字段表示转赠前的code。 */
	private String oldUserCardCode;

	/** 领取场景值，用于领取渠道数据统计。可在生成二维码接口及添加Addcard接口中自定义该字段的字符串值。 */
	private String outerStr;

	/** 用户删除会员卡后可重新找回，当用户本次操作为找回时，该值为1，否则为0 */
	private Boolean isRestoreMemberCard;

	/** 领券用户的UnionId */
	private String unionid;

	/**  */
	private Integer outerId;

	/**  */
	private Boolean isRecommendByFriend;

	/**  */
	private String sourceScene;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId == null ? null : appId.trim();
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId == null ? null : wxId.trim();
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid == null ? null : openid.trim();
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId == null ? null : cardId.trim();
	}

	public String getCardState() {
		return cardState;
	}

	public void setCardState(String cardState) {
		this.cardState = cardState == null ? null : cardState.trim();
	}

	public Boolean getHasActive() {
		return hasActive;
	}

	public void setHasActive(Boolean hasActive) {
		this.hasActive = hasActive;
	}

	public Boolean getIsGiveByFriend() {
		return isGiveByFriend;
	}

	public void setIsGiveByFriend(Boolean isGiveByFriend) {
		this.isGiveByFriend = isGiveByFriend;
	}

	public String getFriendUserName() {
		return friendUserName;
	}

	public void setFriendUserName(String friendUserName) {
		this.friendUserName = friendUserName == null ? null : friendUserName.trim();
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode == null ? null : userCardCode.trim();
	}

	public String getOldUserCardCode() {
		return oldUserCardCode;
	}

	public void setOldUserCardCode(String oldUserCardCode) {
		this.oldUserCardCode = oldUserCardCode == null ? null : oldUserCardCode.trim();
	}

	public String getOuterStr() {
		return outerStr;
	}

	public void setOuterStr(String outerStr) {
		this.outerStr = outerStr == null ? null : outerStr.trim();
	}

	public Boolean getIsRestoreMemberCard() {
		return isRestoreMemberCard;
	}

	public void setIsRestoreMemberCard(Boolean isRestoreMemberCard) {
		this.isRestoreMemberCard = isRestoreMemberCard;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid == null ? null : unionid.trim();
	}

	public Integer getOuterId() {
		return outerId;
	}

	public void setOuterId(Integer outerId) {
		this.outerId = outerId;
	}

	public Boolean getIsRecommendByFriend() {
		return isRecommendByFriend;
	}

	public void setIsRecommendByFriend(Boolean isRecommendByFriend) {
		this.isRecommendByFriend = isRecommendByFriend;
	}

	public String getSourceScene() {
		return sourceScene;
	}

	public void setSourceScene(String sourceScene) {
		this.sourceScene = sourceScene == null ? null : sourceScene.trim();
	}

	@Override
	public String toString() {
		return "UserCard [id=" + id + ", appId=" + appId + ", wxId=" + wxId + ", openid=" + openid + ", cardId="
				+ cardId + ", cardState=" + cardState + ", hasActive=" + hasActive + ", isGiveByFriend="
				+ isGiveByFriend + ", friendUserName=" + friendUserName + ", userCardCode=" + userCardCode
				+ ", oldUserCardCode=" + oldUserCardCode + ", outerStr=" + outerStr + ", isRestoreMemberCard="
				+ isRestoreMemberCard + ", unionid=" + unionid + ", outerId=" + outerId + ", isRecommendByFriend="
				+ isRecommendByFriend + ", sourceScene=" + sourceScene + "]";
	}

}