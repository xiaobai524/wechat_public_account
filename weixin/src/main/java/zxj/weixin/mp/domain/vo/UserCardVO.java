package zxj.weixin.mp.domain.vo;

import zxj.weixin.mp.domain.UserCard;

/**
 * 微信公众号用户领取的会员卡VO
 * 
 * @author zhuxuejiang
 *
 */
public class UserCardVO extends UserCard {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "UserCardVO [getId()=" + getId() + ", getAppId()=" + getAppId() + ", getWxId()=" + getWxId()
				+ ", getOpenid()=" + getOpenid() + ", getCardId()=" + getCardId() + ", getCardState()=" + getCardState()
				+ ", getIsGiveByFriend()=" + getIsGiveByFriend() + ", getFriendUserName()=" + getFriendUserName()
				+ ", getUserCardCode()=" + getUserCardCode() + ", getOldUserCardCode()=" + getOldUserCardCode()
				+ ", getOuterStr()=" + getOuterStr() + ", getIsRestoreMemberCard()=" + getIsRestoreMemberCard()
				+ ", getUnionid()=" + getUnionid() + ", getOuterId()=" + getOuterId() + ", getIsRecommendByFriend()="
				+ getIsRecommendByFriend() + ", getSourceScene()=" + getSourceScene() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}