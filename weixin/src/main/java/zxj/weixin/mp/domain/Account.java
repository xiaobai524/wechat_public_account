package zxj.weixin.mp.domain;

import java.io.Serializable;

/**
 * 微信公众号实体类
 * 
 * @author zhuxuejiang
 *
 */
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 公众号的唯一标识（主键） */
	private String appId;

	/** 微信原始ID */
	private String wxId;

	/** 公众号密钥 */
	private String appSecret;

	/** 公众号令牌 */
	private String token;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "Account [appId=" + appId + ", wxId=" + wxId + ", appSecret=" + appSecret + ", token=" + token + "]";
	}

}
