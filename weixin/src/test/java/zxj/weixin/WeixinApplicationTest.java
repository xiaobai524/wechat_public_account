package zxj.weixin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeixinApplicationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(WeixinApplicationTest.class);

	@Test
	public void contextLoads() {
		LOGGER.debug("Weixin public account !");
	}

}
