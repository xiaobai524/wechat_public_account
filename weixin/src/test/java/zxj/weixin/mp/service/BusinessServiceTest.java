package zxj.weixin.mp.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import zxj.weixin.mp.domain.Business;

/**
 * 微信门店Service测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessServiceTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	BusinessService businessService;

	/**
	 * 创建门店测试方法（测试号无此接口权限）
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateBusiness() throws Exception {
		Business business = new Business();
		business.setAppId(appId);
		business.setBusinessName("ZXJ调试门店");
		business.setBranchName("ZXJ调试分店-1");
		business.setProvince("黑龙江省");
		business.setCity("哈尔滨市");
		business.setDistrict("香坊区");
		business.setAddress("嵩山路天源石化");
		business.setTelephone("15645198009");
		business.setOffsetType((byte) 1);
		business.setLongitude(115.32375);
		business.setLatitude(25.097486);

		business.setIntroduction("超级NB的加油站！！！");
		business.setRecommend("#92汽油、#95汽油、#98汽油、#0柴油。");
		business.setSpecial("免费wifi。");
		business.setOpenTime("8:00-17:00");
		business.setAvgPrice(28);

		List<String> categorieList = new ArrayList<>(3);
		categorieList.add("美食,小吃快餐");

		List<String> photoUrlList = new ArrayList<>(3);
		photoUrlList.add(
				"http://mmbiz.qpic.cn/mmbiz_jpg/iaa9VRPCyfMC9KJwVk7mtyDfQ0fhgmAqsWoc2g8hBrZBgKKBo9dCOCYiaZ2BibA1qLEqnCllDLhDOl2LPnT7moEmA/0");
		photoUrlList.add(
				"http://mmbiz.qpic.cn/mmbiz_jpg/iaa9VRPCyfMC9KJwVk7mtyDfQ0fhgmAqsWoc2g8hBrZBgKKBo9dCOCYiaZ2BibA1qLEqnCllDLhDOl2LPnT7moEmA/0");
		photoUrlList.add(
				"http://mmbiz.qpic.cn/mmbiz_jpg/iaa9VRPCyfMC9KJwVk7mtyDfQ0fhgmAqsWoc2g8hBrZBgKKBo9dCOCYiaZ2BibA1qLEqnCllDLhDOl2LPnT7moEmA/0");

		businessService.createBusiness(business, categorieList, photoUrlList);
	}

	/**
	 * 获取门店类目列表测试方法（测试号无此接口权限）
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetCategoryList() throws Exception {
		System.out.println(businessService.getCategoryList(appId));
	}

}
