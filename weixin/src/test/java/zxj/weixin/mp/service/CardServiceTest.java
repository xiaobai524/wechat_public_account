package zxj.weixin.mp.service;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * 微信卡券Service测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CardServiceTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	CardService cardService;

	/**
	 * 获取api_ticket调用卡券相关接口的临时票据测试方法
	 * @throws Exception 
	 */
	@Test
	public void testGetApiTicket() throws Exception {
		String apiTicker = cardService.getApiTicket(appId);
		Assert.notNull(apiTicker, apiTicker);
		System.out.println(apiTicker);
	}

	/**
	 * 创建二维码测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateQrCode() throws Exception {
		String cardId = "pEn7ks9CVVa1mci1HEhhzqbhVRng";
		Map<String, Object> map = cardService.createQrCode(appId, cardId);
		Assert.notNull(map, map.toString());
		System.out.println(map);
	}

}
