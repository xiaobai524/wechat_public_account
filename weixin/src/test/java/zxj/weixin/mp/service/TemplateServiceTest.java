package zxj.weixin.mp.service;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * 微信门店Service测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TemplateServiceTest {

	private String openid = "o99crwkDghDcLNl9qP7IhmlKNoT8";

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	TemplateService templateService;

	/**
	 * 发送会员注册成功通知测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSendMemberRegisterSuccessNotice() throws Exception {
		String title = "测试会员卡";
		String name = "朱学江";
		String mobile = "13123456789";
		int bonus = 100;
		String level = "白银会员";
		String code = "951404641297";
		String url = "https://mp.weixin.qq.com";

		Map<String, Object> resultMap = templateService.sendMemberRegisterSuccessNotice(appId, openid, title, name,
				mobile, bonus, level, code, url);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 发送会员卡激活成功通知测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSendMemberCardActivationSuccessNotice() throws Exception {
		String title = "测试会员卡";
		String code = "951404641297";
		String time = "2018-12-22 09:28:45";
		String url = "https://mp.weixin.qq.com";

		Map<String, Object> resultMap = templateService.sendMemberCardActivationSuccessNotice(appId, openid, title,
				code, time, url);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

}
