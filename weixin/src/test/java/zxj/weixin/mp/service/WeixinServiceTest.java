package zxj.weixin.mp.service;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * 微信公众号基础功能Service测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeixinServiceTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 获取access_token测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetAccessToken() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		Assert.notNull(accessToken, accessToken);
		System.out.println(accessToken);
	}

	/**
	 * 获取jsapi_ticket测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetJsapiTicket() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		String jsapiTicket = weixinService.getJsapiTicket(appId, accessToken);
		Assert.notNull(jsapiTicket, jsapiTicket);
		System.out.println(jsapiTicket);
	}

	/**
	 * 通过code换取网页授权access_token测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetWebAuthAccessToken() throws Exception {
		String code = "";
		Map<String, Object> map = weixinService.getWebAuthAccessToken(appId, code);
		Assert.notNull(map, map.toString());
		System.out.println(map.toString());
	}

}
