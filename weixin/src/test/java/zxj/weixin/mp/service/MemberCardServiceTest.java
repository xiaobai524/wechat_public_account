package zxj.weixin.mp.service;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import zxj.weixin.mp.api.entity.MemberCardUserInfo;
import zxj.weixin.mp.domain.CardBaseInfo;
import zxj.weixin.mp.domain.MemberCard;

/**
 * 微信会员卡Service测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberCardServiceTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	MemberCardService memberCardService;

	/**
	 * 创建会员卡测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreate() throws Exception {
		MemberCard memberCard = new MemberCard();
		memberCard.setAppId(appId);
		memberCard.setBackgroundPicUrl(
				"http://mmbiz.qpic.cn/mmbiz_jpg/iaa9VRPCyfMC9KJwVk7mtyDfQ0fhgmAqsWoc2g8hBrZBgKKBo9dCOCYiaZ2BibA1qLEqnCllDLhDOl2LPnT7moEmA/0");
		memberCard.setPrerogative("会员卡特权说明，限制1024汉字");
		memberCard.setSupplyBonus(true);
		memberCard.setBonusUrl("https://mp.weixin.qq.com");
		memberCard.setSupplyBalance(true);
		memberCard.setBalanceUrl("https://mp.weixin.qq.com/wiki");
		// memberCard.setAutoActivate(true);
		memberCard.setWxActivate(true);

		CardBaseInfo cardBaseInfo = new CardBaseInfo();
		cardBaseInfo.setCheckState("card_checking");
		cardBaseInfo.setCardType("MEMBER_CARD");
		cardBaseInfo.setColor("Color010");
		cardBaseInfo.setLogoUrl(
				"http://mmbiz.qpic.cn/mmbiz_png/iaa9VRPCyfMC9KJwVk7mtyDfQ0fhgmAqsP8ALW2osTjl1uSqnEia1ZD8SMyp2NIxS250Gz0wvUiafOXyQTrwW8A1A/0");
		cardBaseInfo.setCodeType("CODE_TYPE_QRCODE");
		cardBaseInfo.setBrandName("ZXJ会员卡");
		cardBaseInfo.setTitle("普通一键激活");
		cardBaseInfo.setNotice("请到店出示此卡，享受会员优惠！");
		cardBaseInfo.setDescription("卡券使用说明，1024个汉字");
		cardBaseInfo.setSkuQuantity(100000000);
		cardBaseInfo.setDateInfoType("DATE_TYPE_PERMANENT");
		cardBaseInfo.setGetLimit(1);
		cardBaseInfo.setUseLimit(1);
		cardBaseInfo.setCanShare(false);
		cardBaseInfo.setCanGiveFriend(false);
		cardBaseInfo.setIsSwipeCard(true);
		cardBaseInfo.setIsPayAndQrcode(true);

		memberCardService.create(memberCard, cardBaseInfo);
	}

	/**
	 * 拉取会员信息测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testGetMemberCardUserInfo() throws IOException, Exception {
		String cardId = "pEn7ks2k3b1Vx8nYDSVoDRbfCQO0";
		String userCardCode = "444467523504";
		MemberCardUserInfo memberCardUserInfo = memberCardService.getMemberCardUserInfo(appId, cardId, userCardCode);
		Assert.notNull(memberCardUserInfo, memberCardUserInfo.toString());
		System.out.println(memberCardUserInfo);
	}

}
