package zxj.weixin.mp.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import zxj.weixin.mp.domain.CardBaseInfo;
import zxj.weixin.mp.domain.MemberCard;
import zxj.weixin.mp.domain.MemberCardCellField;
import zxj.weixin.mp.domain.MemberCardCustomField;
import zxj.weixin.mp.service.WeixinService;
import zxj.weixin.utils.MapUtils;

/**
 * 微信卡券API测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CardApiTest {

	private static final String DATE_TYPE_FIX_TIME_RANGE = "DATE_TYPE_FIX_TIME_RANGE";
	private static final String DATE_TYPE_FIX_TERM = "DATE_TYPE_FIX_TERM";

	private static final String LOGO_URL = "http://mmbiz.qpic.cn/mmbiz_jpg/iaa9VRPCyfMDDtv7Y9HwtLgbL8icUicv89v4h0HxGLKiblO6WwJHDSpPgIhVsNCeyiaicAAMmMcnvcxqoFvkcYZGu3Ag/0";
	private static final String BACKGROUND_URL = "http://mmbiz.qpic.cn/mmbiz_png/iaa9VRPCyfMDDtv7Y9HwtLgbL8icUicv89vJpBYiayTWwz2ibEl19S1Fujl27Ec1jlZ1HhZEguXdN7PCXvT61kSBKLA/0";

	private static final String CARD_ID = "pEn7ks9U2M2jch0m0EeG3pK3CPSc";
	private static final String CARD_CODE = "444467523504";

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 创建微信会员卡信息
	 * 
	 * @return
	 * @throws IOException
	 */
	private Map<String, Object> createMemberMap() throws IOException {
		MemberCard memberCard = new MemberCard();
//		memberCard.setAppId(appId);
		memberCard.setBackgroundPicUrl(BACKGROUND_URL);
		memberCard.setPrerogative("特权说明");
		memberCard.setSupplyBonus(true);
		memberCard.setBonusCleared("积分清零规则");
		memberCard.setBonusRules("积分规则");
		memberCard.setBonusUrl("https://mp.weixin.qq.com");
		// 无储值功能的不要设置supply_balance=true
		memberCard.setSupplyBalance(false);
//		memberCard.setBalanceUrl("https://mp.weixin.qq.com/wiki");
		memberCard.setWxActivate(true);
		memberCard.setWxActivateAfterSubmit(true);
		memberCard.setWxActivateAfterSubmitUrl("http://zxj.ngrok.xiaomiqiu.cn/member_card_activate.html");
		memberCard.setAutoActivate(false);
		memberCard.setActivateUrl(null);
		memberCard.setActivateAppBrandUserName(null);
		memberCard.setActivateAppBrandPass(null);
		memberCard.setDiscount(9);

		return MapUtils.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(memberCard));
	}

	private CardBaseInfo createCardBaseInfo() {
		CardBaseInfo cardBaseInfo = new CardBaseInfo();
		// 必填字段
		cardBaseInfo.setCardType("MEMBER_CARD");
		cardBaseInfo.setLogoUrl(LOGO_URL);
		cardBaseInfo.setCodeType("CODE_TYPE_QRCODE");
		cardBaseInfo.setBrandName("调试用会员卡");
		cardBaseInfo.setTitle("跳转型一键激活");
		cardBaseInfo.setColor("Color010");
		cardBaseInfo.setNotice("卡券使用提醒");
		cardBaseInfo.setDescription("卡券使用说明");
		cardBaseInfo.setSkuQuantity(100000000);
		cardBaseInfo.setDateInfoType("DATE_TYPE_PERMANENT");
		cardBaseInfo.setBeginTimestamp(null);
		cardBaseInfo.setEndTimestamp(null);
		cardBaseInfo.setFixedBeginTerm(null);
		cardBaseInfo.setFixedTerm(null);
		// 非必填字段
		cardBaseInfo.setUseCustomCode(false);
		cardBaseInfo.setGetCustomCodeMode(null);
		cardBaseInfo.setBindOpenid(false);
		cardBaseInfo.setServicePhone("51106666");
		cardBaseInfo.setUseAllLocations(true);
//		cardBaseInfo.setCenterTitle("居中按钮");
//		cardBaseInfo.setCenterSubTitle("下方提示语");
//		cardBaseInfo.setCenterUrl("http://www.taobao.com");
//		cardBaseInfo.setCenterAppBrandUserName(null);
//		cardBaseInfo.setCenterAppBrandPass(null);
//		cardBaseInfo.setCustomUrlName("外链入口");
//		cardBaseInfo.setCustomUrl("http://www.tmall.com");
//		cardBaseInfo.setCustomUrlSubTitle("右侧提示语");
//		cardBaseInfo.setCustomAppBrandUserName(null);
//		cardBaseInfo.setCustomAppBrandPass(null);
//		cardBaseInfo.setPromotionUrlName("营销场景");
//		cardBaseInfo.setPromotionUrl("http://www.jd.com");
//		cardBaseInfo.setPromotionUrlSubTitle("营销提示语");
//		cardBaseInfo.setPromotionAppBrandUserName(null);
//		cardBaseInfo.setPromotionAppBrandPass(null);
		cardBaseInfo.setGetLimit(1);
		cardBaseInfo.setUseLimit(1);
		cardBaseInfo.setCanShare(false);
		cardBaseInfo.setCanGiveFriend(false);
		cardBaseInfo.setIsSwipeCard(true);
		cardBaseInfo.setIsPayAndQrcode(true);
		cardBaseInfo.setNeedPushOnView(true);
		return cardBaseInfo;
	}

	private Map<String, Object> createBaseInfoMap() throws IOException {
		Map<String, Object> baseInfoMap = MapUtils
				.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(this.createCardBaseInfo()));

		// 处理卡券库存的数量
		Integer quantity = (Integer) baseInfoMap.get("sku_quantity");
		if (null != quantity) {
			Map<String, Integer> skuMap = new HashMap<>(16);
			skuMap.put("quantity", quantity);
			baseInfoMap.put("sku", skuMap);
		}

		// 处理使用日期，有效期的信息
		String dateInfoType = (String) baseInfoMap.get("date_info_type");
		if (StringUtils.isNotBlank(dateInfoType)) {
			Map<String, Object> dateInfoMap = new HashMap<>(16);
			dateInfoMap.put("type", baseInfoMap.get("date_info_type"));

			if (dateInfoType.equalsIgnoreCase(DATE_TYPE_FIX_TIME_RANGE)) {
				dateInfoMap.put("begin_timestamp", baseInfoMap.get("begin_timestamp"));
				dateInfoMap.put("end_timestamp", baseInfoMap.get("end_timestamp"));
			} else if (dateInfoType.equalsIgnoreCase(DATE_TYPE_FIX_TERM)) {
				dateInfoMap.put("fixed_term", baseInfoMap.get("fixed_term"));
				dateInfoMap.put("fixed_begin_term", baseInfoMap.get("fixed_begin_term"));
				dateInfoMap.put("end_timestamp", baseInfoMap.get("end_timestamp"));
			}
			baseInfoMap.put("date_info", dateInfoMap);
		}

		// 处理
		Boolean isSwipeCard = (Boolean) baseInfoMap.get("is_swipe_card");
		if (isSwipeCard) {
			Map<String, Object> swipeCardMap = new HashMap<>(16);
			swipeCardMap.put("is_swipe_card", true);

			Map<String, Object> payInfoMap = new HashMap<>(16);
			payInfoMap.put("swipe_card", swipeCardMap);

			baseInfoMap.put("pay_info", payInfoMap);
		}

		baseInfoMap.remove("card_id");
		baseInfoMap.remove("check_state");
		baseInfoMap.remove("refuse_reason");
		baseInfoMap.remove("card_type");

		baseInfoMap.remove("sku_quantity");
		baseInfoMap.remove("date_info_type");
		baseInfoMap.remove("begin_timestamp");
		baseInfoMap.remove("end_timestamp");
		baseInfoMap.remove("fixed_term");
		baseInfoMap.remove("fixed_begin_term");
		baseInfoMap.remove("is_swipe_card");

		return baseInfoMap;
	}

	/**
	 * 创建卡券测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreate() throws Exception {

		Map<String, Object> memberCardMap = this.createMemberMap();
		memberCardMap.put("base_info", this.createBaseInfoMap());

		// 自定义会员信息类目（背景图片下横向显示的）
		MemberCardCustomField memberCardCustomField = new MemberCardCustomField();
		memberCardCustomField.setNameType("FIELD_NAME_TYPE_LEVEL");
		memberCardCustomField.setName("会员等级");
		memberCardCustomField.setUrl("http://www.qq.com");
		memberCardMap.put("custom_field1",
				MapUtils.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(memberCardCustomField)));

		// 自定义会员信息类目（“会员卡详情”下纵向显示的）
		MemberCardCellField memberCardCellField = new MemberCardCellField();
		memberCardCellField.setName("最新活动");
		memberCardCellField.setTips("最新活动提示");
		memberCardCellField.setUrl("https://www.baidu.com/");
		memberCardMap.put("custom_cell1",
				MapUtils.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(memberCardCellField)));

		// 积分规则
//		MemberCardBonusRule memberCardBonusRule = new MemberCardBonusRule();
//		memberCardBonusRule.setCostMoneyUnit(100);
//		memberCardBonusRule.setIncreaseBonus(10);
//		memberCardBonusRule.setMaxIncreaseBonus(50);
//		memberCardBonusRule.setInitIncreaseBonus(500);
//		memberCardBonusRule.setCostBonusUnit(200);
//		memberCardBonusRule.setMaxReduceBonus(20);
//		memberCardBonusRule.setLeastMoneyToUseBonus(300);
//		memberCardBonusRule.setMaxReduceBonus(400);
//		memberCardMap.put("bonus_rule",
//				MapUtils.keyLowerCamel2LowerUnderScore(MapUtils.object2MapNonNull(memberCardBonusRule)));

		Map<String, Object> cardMap = new HashMap<>(16);
		cardMap.put("card_type", "MEMBER_CARD");
		cardMap.put("member_card", memberCardMap);

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card", cardMap);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.create(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 创建二维码测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateQrCode() throws Exception {
		Map<String, Object> cardMap = new HashMap<>(16);
		cardMap.put("card_id", CARD_ID);

		Map<String, Object> actionInfoMap = new HashMap<>(16);
		actionInfoMap.put("card", cardMap);

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("action_name", "QR_CARD");
		dataMap.put("action_info", actionInfoMap);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.createQrCode(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 通过卡券货架投放卡券-创建货架测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreateLandingpage() throws Exception {

		List<Map<String, Object>> cardList = new ArrayList<>();

		Map<String, Object> cardMap = new HashMap<>(16);
		cardMap.put("card_id", CARD_ID);
		cardMap.put("thumb_url", LOGO_URL);
		cardList.add(cardMap);

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("banner", BACKGROUND_URL);
		dataMap.put("page_title", "ZXJ会员卡");
		dataMap.put("can_share", false);
		dataMap.put("scene", "SCENE_H5");
		dataMap.put("card_list", cardList);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.createLandingpage(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取图文消息群发卡券html测试方法
	 * 
	 * @throws Exception
	 * @throws IOException
	 */
	@Test
	public void testGetHtmlMpNews() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.getHtmlMpNews(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 设置测试白名单测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetTestWhiteList() throws Exception {
		List<String> openidList = new ArrayList<>();
		openidList.add("oEn7ks931CFpB_Ktx-InmHcCb6L4");

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("openid", openidList);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.setTestWhiteList(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取用户已领取卡券测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetUserCardList() throws Exception {

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("openid", "oEn7ks931CFpB_Ktx-InmHcCb6L4");
		dataMap.put("card_id", CARD_ID);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.getUserCardList(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 批量查询卡券列表测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testBatchGet() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("offset", 0);
		dataMap.put("count", 50);

//		List<String> statusList = new ArrayList<>();
//		// 待审核
//		statusList.add("CARD_STATUS_NOT_VERIFY");
//		// 审核失败
//		statusList.add("CARD_STATUS_VERIFY_FAIL");
//		// 通过审核
//		statusList.add("CARD_STATUS_VERIFY_OK");
//		// 卡券被商户删除
//		statusList.add("CARD_STATUS_DELETE");
//		// 在公众平台投放过的卡券
//		statusList.add("CARD_STATUS_DISPATCH");
//		dataMap.put("status_list", statusList);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.batchGet(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 更改卡券信息测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testUpdate() throws IOException, Exception {

		Map<String, Object> memberCardMap = this.createMemberMap();

		Map<String, Object> baseInfoMap = this.createBaseInfoMap();

		// 更改时这个几个属性一定要remove
		baseInfoMap.remove("brand_name");
		baseInfoMap.remove("use_custom_code");
		baseInfoMap.remove("sku");

		memberCardMap.put("base_info", baseInfoMap);

		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);
		dataMap.put("member_card", memberCardMap);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.update(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 删除卡券信息测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testDelete() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.delete(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 删除全部卡券信息测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDeleteAll() throws Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("offset", 0);
		dataMap.put("count", 50);

		System.out.println(MapUtils.map2JsonString(dataMap));

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.batchGet(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);

		List<String> cardIdList = (List<String>) resultMap.get("card_id_list");
		Assert.notNull(cardIdList, cardIdList.toString());
		System.out.println(cardIdList.size());

		for (String cardId : cardIdList) {
			dataMap.clear();
			dataMap.put("card_id", cardId);

			resultMap = CardApi.delete(accessToken, dataMap);
			Assert.notNull(resultMap, resultMap.toString());
			System.out.println(resultMap);
		}
	}

	/**
	 * 设置开卡字段测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testSetMemberCardActivateUserForm() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);

		Map<String, Object> requiredFormMap = new HashMap<>(16);
		requiredFormMap.put("can_modify", true);

		List<String> commonFieldIdList = new ArrayList<>();
		commonFieldIdList.add("USER_FORM_INFO_FLAG_MOBILE");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_SEX");
		commonFieldIdList.add("USER_FORM_INFO_FLAG_NAME");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_BIRTHDAY");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_IDCARD");

//		commonFieldIdList.add("USER_FORM_INFO_FLAG_EMAIL");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_INDUSTRY");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_INCOME");
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_HABIT");

		// 地址，测试时iPhone 6点击输入框后，无法调起键盘。
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_LOCATION");

		// 教育背景，设置后报错
//		commonFieldIdList.add("USER_FORM_INFO_FLAG_EDUCATION_BACKGRO");

		requiredFormMap.put("common_field_id_list", commonFieldIdList);

		dataMap.put("required_form", requiredFormMap);

		Map<String, Object> serviceStatementMap = new HashMap<>(16);
		serviceStatementMap.put("name", "会员守则");
		serviceStatementMap.put("url", "http://www.baidu.com");

		dataMap.put("service_statement", serviceStatementMap);

		Map<String, Object> bindOldCardMap = new HashMap<>(16);
		bindOldCardMap.put("name", "老会员绑定");
		bindOldCardMap.put("url", "https://weixin.qq.com/");

		dataMap.put("bind_old_card", bindOldCardMap);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.setMemberCardActivateUserForm(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取开卡组件链接测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testGetMemberCardActivateUrl() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);
		dataMap.put("outer_str", "H5");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.getMemberCardActivateUrl(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 激活会员卡测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testActivateMemberCard() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);
		dataMap.put("code", CARD_CODE);
//		dataMap.put("membership_number", "845883450874");
		dataMap.put("init_bonus", 123);
		dataMap.put("init_bonus_record", "初始化积分");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.activateMemberCard(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 拉取会员信息（积分查询）测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testGetMemberCardUserInfo() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);
		dataMap.put("code", CARD_CODE);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.getMemberCardUserInfo(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 更新会员信息测试方法
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	@Test
	public void testUpdateMemberCardUser() throws IOException, Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("card_id", CARD_ID);
		dataMap.put("code", CARD_CODE);
		dataMap.put("background_pic_url",
				"http://mmbiz.qpic.cn/mmbiz_jpg/LLIBEG1Loo43lfjRU5BgHDPjsRoVu8RMtXlo8vC0sT94AstpSmF5diapu7ibp0FxJObIO4v3176uMLJrlWK1pD0Q/0");
		dataMap.put("bonus", 10);
		dataMap.put("add_bonus", 10);
		dataMap.put("record_bonus", "消费10元，获得10积分");
		/*
		 * balance 和 add_balance可能是未开通储值功能
		 * 返回：{errcode=45023, errmsg=balance is out of limit hint: [Y7Db1a07155344]}
		 */

//		dataMap.put("balance", 90);
//		dataMap.put("add_balance", -10);
		dataMap.put("record_balance", "消费扣除金额10元。");
		dataMap.put("custom_field_value1", "普通会员");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = CardApi.updateMemberCardUser(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

}
