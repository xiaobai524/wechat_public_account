package zxj.weixin.mp.api;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import zxj.weixin.mp.service.WeixinService;

/**
 * 微信JS-SDK API测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JsSdkApiTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 获取jsapi_ticket测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetJsapiTicket() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = JsSdkApi.getJsapiTicket(appId, accessToken);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取api_ticket调用卡券相关接口的临时票据测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetWxCardTicket() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = JsSdkApi.getWxCardTicket(accessToken);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

}
