package zxj.weixin.mp.api;

import java.io.File;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import zxj.weixin.mp.service.WeixinService;

/**
 * 微信公众号素材管理API测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MaterialApiTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 上传图文消息内的图片获取URL测试方法
	 * 
	 * @throws Exception
	 */

	@Test
	public void testMediaUploadImg() throws Exception {
		File imgFile = new File("d:\\logo.jpg");
		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = MaterialApi.mediaUploadImg(accessToken, imgFile);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

}
