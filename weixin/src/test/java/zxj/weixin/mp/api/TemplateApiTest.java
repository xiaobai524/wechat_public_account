package zxj.weixin.mp.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import zxj.weixin.mp.service.WeixinService;

/**
 * 微信模版消息API测试类
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TemplateApiTest {

	@Value("${weixin.test.appId}")
	private String appId;

	@Autowired
	private WeixinService weixinService;

	/**
	 * 设置所属行业测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIndustry() throws Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("industry_id1", "1");
		dataMap.put("industry_id2", "41");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.setIndustry(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取设置的行业信息测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIndustry() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.getIndustry(accessToken);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获得模板ID测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAddTemplate() throws Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("template_id_short", "TM00015");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.addTemplate(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 获取模板列表测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetAllPrivateTemplate() throws Exception {
		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.getAllPrivateTemplate(accessToken);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 删除模版测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testDelPrivateTemplate() throws Exception {
		Map<String, Object> dataMap = new HashMap<>(16);
		dataMap.put("template_id", "omCQSnXfvXRT0OMzS32oCNd8Cdv5oBl1AqJ_9ICLzMA");

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.delPrivateTemplate(accessToken, dataMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

	/**
	 * 发送模板消息测试方法
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSend() throws Exception {
		Map<String, Object> postMap = new HashMap<>(16);
		postMap.put("touser", "oEn7ks931CFpB_Ktx-InmHcCb6L4");
		postMap.put("template_id", "4t684bLWMY85feXjeVzKqsn1la-5Oox65sY1suwcdgQ");
		postMap.put("url", "http://www.baidu.com");

		Map<String, Object> dataMap = new HashMap<>(16);

		Map<String, Object> valueMap = new HashMap<>(16);
		valueMap.put("value", "测试测试测试");
		dataMap.put("first", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", "100元");
		dataMap.put("orderMoneySum", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", "测试的商品名称");
		dataMap.put("orderProductName", valueMap);

		valueMap = new HashMap<>(16);
		valueMap.put("value", "测试的备注信息");
		dataMap.put("Remark", valueMap);

		postMap.put("data", dataMap);

		String accessToken = weixinService.getAccessToken(appId);
		Map<String, Object> resultMap = TemplateApi.send(accessToken, postMap);
		Assert.notNull(resultMap, resultMap.toString());
		System.out.println(resultMap);
	}

}
