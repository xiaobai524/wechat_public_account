package zxj.weixin.mp.mongo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import zxj.weixin.mp.domain.message.TextMsg;

/**
 * 
 * @author zhuxuejiang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TextMsgDaoTest {

	@Autowired
	private TextMsgDao textMsgDao;

	@Test
	public void testSave() {
		TextMsg testMsg = new TextMsg();
		testMsg.setMsgId(1L);
		testMsg.setToUserName("ToUserName");
		testMsg.setFromUserName("FromUserName");
		testMsg.setMsgType("text");
		testMsg.setCreateTime(2L);
		testMsg.setContent("Content");

		textMsgDao.save(testMsg);
	}

}
